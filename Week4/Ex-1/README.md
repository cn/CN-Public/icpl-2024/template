# Exercises E2.3 to E2.6 (Graded all together)

Your task is to complete the compiler from L<sub>Var</sub> to x86<sub>Int</sub> as described in Exercises 2.3, 2.4, 2.5 and 2.6 of the book. The grammars for both languages are available in the slides and the reference book for your compatibility checks. 

To evaluate your compiled programs, from this week we will establish a test mechanism in which you will provide tests written in the source language in a **.py** file, a corresponding example input in a  **.in** file, and a **.golden** file in which you write the expected output of the program.

For example, in the following you see a test case written in a file `t0_print_input_add.py` in the L<sub>Var</sub>, as our compiler compiles programs from L<sub>Var</sub> language:

```Python
print(input_int() + 1)

```

The input behavior of this test must be written in `t0_print_input_add.in` file. As `t0_print_input_add.py` only gets an integer from user by calling **input_int** function and 42 is a valid input, then one possible content for `t0_print_input_add.in` would be as follows:

```Python
42
```
And finally, the content of the file `t0_print_input_add.golden` which shows the valid output of the execution of the program, which should be the same whether it is executed on the L<sub>Var</sub> interpreter or executed as a binary executable after compilation. For `t0_print_input_add.py` the expected output is:

```Python
43
```


1. **Complete the Compiler class in the template code**: Complete the functions *remove_complex_operands, select_instructions, assign_homes, patch_instructions* and *prelude_and_conclusion*. You have already implemented remove_complex_operands and you can copy it here. As we don't cover register allocation you can implement the behavior of *patch_instructions* and *prelude_and_conclusion* functions inside the *assign_homes* function and keep them as identity functions, i.e. that return the same program without modifying it, or you can implement the corresponding behavior in each function separately.

2. **Provide 12 Test Cases**: write 12 different test cases in the format **t{your_team_number}_{short_descriptive_name}**. Each test should be as small as possible while sufficiently complex to correctly exercise a unique case that your compiler supports and allow detection of bugs through the input-output behaviour of your test.  For each test you should provide 3 files: 1) `t{your_team_number}_{short_descriptive_name}.py`, which would be written in L<sub>Var</sub>; 2) `t{i}_{short_descriptive_name}.in`, which provides an example of input; and 3) `t{i}_{short_descriptive_name}.golden`, which provides the expected output.

3. **Ensure all your tests are correctly executed using the test program (`run_tests.py`) provided in the template** : We will use a copy of the same script to grade your tests and compiler. If you find a bug or encounter issues in `run_tests.py`, please share them (early) on the Discord forum and we will update the template as necessary. See usage example below.

## Requirements

- `diff` and `gcc` should be installed and available in the PATH
- Python 3 (successfully tested with 3.12.3)

## Usage example

### Run a single test

```
    python3 run_tests.py tests/t0_print_input_add.py
```

The default output using the compiler.py file of the template should look like this:
```
0a1
> 43
compiler Var failed pass remove_complex_operands on test:
tests/t0_print_input_add

tests/t0_print_input_add.s: Assembler messages:
tests/t0_print_input_add.s: Warning: end of file not at end of a line; newline inserted
tests/t0_print_input_add.s:1: Error: no such instruction: `none'
sh: 1: ./a.out: not found
0a1
> 43
compiler Var, executable failed on test tests/t0_print_input_add
0/5 passes successful, test failed
```

A successful output would be:
```
2/4 passes successful, test success
```

You can ignore the 2 passes that are not successful, these are the intermediate x86 transformations that we do not evaluate. The most important part is that the final test is a success, which means that compilation and execution of the x86 executable succeeded.

### Run all tests in directory

```
    python3 run_tests.py tests
```

A successful output would be:
```
tests: 1/1 for compiler Var
passes: 2/4 for compiler Var
```

Again you can ignore the two missing passes.

### Run all tests but stop at the first failed pass or result

```
    python3 run_tests.py tests --strict
```

This is useful for debugging. For those using `Vim`, you can directly use it to visualize the difference between the expected and actual output and automatically open on error by adding the following at 1177 in utils.py:

```
1176             if stop_on_failed:
1177                 os.system('vim -d ' + program_root + '.out ' + program_root + '.golden')
1178                 sys.exit(1)  
```

### Run a test while showing intermediary compilation results

```
    python3 run_tests.py tests/t0_print_input_add.py --trace
```

An example of a successful output would be:

```

# source program: t0_print_input_add

    print((input_int() + 1))


Module([  Expr(Call(Name('print'), [BinOp(Call(Name('input_int'), []), Add(), Constant(1))]))])


# remove_complex_operands

    _t1 = input_int()
    _t2 = (_t1 + 1)
    print(_t2)

Module([  Assign([Name('_t1')], Call(Name('input_int'), [])),   Assign([Name('_t2')], BinOp(Name('_t1'), Add(), Constant(1))),   Expr(Call(Name('print'), [Name('_t2')]))])
compiler Var success on pass remove_complex_operands on test
tests/t0_print_input_add


# select_instructions

	.globl main
main:
    callq read_int
    movq %rax, _t1
    movq $1, %rax
    addq _t1, %rax
    movq %rax, _t2
    movq _t2, %rdi
    callq print


compiler Var skip test on pass select_instructions on test
tests/t0_print_input_add


# assign_homes

	.globl main
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $16, %rsp
    callq read_int
    movq %rax, -8(%rbp)
    movq $1, %rax
    addq -8(%rbp), %rax
    movq %rax, -16(%rbp)
    movq -16(%rbp), %rdi
    callq print_int
    addq $16, %rsp
    popq %rbp
    retq


compiler Var skip test on pass assign_homes on test
tests/t0_print_input_add


# prelude_and_conclusion

	.globl main
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $16, %rsp
    callq read_int
    movq %rax, -8(%rbp)
    movq $1, %rax
    addq -8(%rbp), %rax
    movq %rax, -16(%rbp)
    movq -16(%rbp), %rdi
    callq print_int
    addq $16, %rsp
    popq %rbp
    retq
```


