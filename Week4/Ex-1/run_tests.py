import traceback
from ast import*
from interpreter import interp_Lvar
from compiler import Var
import utils
import argparse
import sys
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Command-line tester for compiler')
    parser.add_argument('path', type=str, help='Python program for a single test or directory of tests')
    parser.add_argument('--trace', action='store_true', help='Show intermediary representations and test results')
    parser.add_argument('--strict', action='store_true', help='Stop on first pass test failure', default=False)

    if not os.path.isfile('runtime.o'):
        sys.stderr.write("runtime.o: not found, please compile runtime.c first (ex: gcc -c runtime.c -o runtime.o )\n")
        sys.exit(1)

    if os.path.isfile('a.out'):
        sys.stderr.write("a.out left from previous test run, please remove first.\n")
        sys.exit(1)

    args = parser.parse_args()

    if args.trace:
        utils.enable_tracing()

    interp_dict = {
        'remove_complex_operands': interp_Lvar,
    }

    if os.path.isfile(args.path):
        (succ_passes, nb_passes, succ_test) = utils.run_one_test(args.path, Var(), 'Var', {}, interp_dict, stop_on_failed=args.strict)
        print("{}/{} passes successful, test {}".format(
            succ_passes, 
            nb_passes, 
            "success" if succ_test == 1 else "failed"))
    elif os.path.isdir(args.path):
        utils.run_tests(args.path, Var(), 'Var', {}, interp_dict, stop_on_failed=args.strict)
    else:
        sys.stderr.write("Invalid path '" + args.path + "', neither a file or directory")
        sys.exit(1)

