import ast
from ast import *
from utils import *
from x86_ast import *
from typing import List, Set, Dict, Tuple

Binding = Tuple[Name, expr]
Temporaries = List[Binding]
_DV_ = 'DUMMY_VARIABLE'

class Var:
    
    ############################################################################
    # Remove Complex Operands
    ############################################################################

    def remove_complex_operands(self, p: Module) -> Module:
        pass

    ############################################################################
    # Select Instructions
    ############################################################################

    def select_instructions(self, p: Module) -> X86Program:
        pass

    ############################################################################
    # Assign Homes
    ############################################################################

    def assign_homes(self, p: X86Program) -> X86Program:
        pass

    ############################################################################
    # Patch Instructions
    ############################################################################

    def patch_instructions(self, p: X86Program) -> X86Program:
        pass

    ############################################################################
    # Prelude & Conclusion
    ############################################################################

    def prelude_and_conclusion(self, p: X86Program) -> X86Program:
        pass
