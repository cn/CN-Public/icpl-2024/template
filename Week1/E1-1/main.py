import ast
from ast import *
from compiler import pe_P_int
from unparse import unparse
from interpreter import interp_Lint

# In the following there is a template for a very simple program
# Ex 1.1 asks you to provide 3 more non trivial examples

def test(srcCode):
    prg1 = ast.parse(srcCode)
    print ('Interpreting before PE ...')
    interp_Lint(prg1)

    prg2 = pe_P_int(prg1) # partial evaluation
    print ('Interpreting after PE ...')
    interp_Lint(prg2)

    peCode = unparse(prg2)
    print("Original source code is: " + srcCode)
    print("Partially evaluated code is: " + peCode)
    print("*"*10)

test('42')

