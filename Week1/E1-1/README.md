# Exercise 1.1

In this exercise, you are required to complete Exercise 1.1 from the reference book. In the file `main.py`, a function called `test` is defined, which takes source code for programs written in L<sub>Int</sub> language, interprets them, compiles (partially evaluates) them, and displays the concrete syntax of the compiled program. Your tasks are as follows:

1. **Complete the Compiler and Interpreter**: Implement the source code for both the compiler and the interpreter. You can find the necessary code in the book or the slides.

2. **Write and Test Programs**: Create  <mark>three non-trivial programs</mark> in the L<sub>Int</sub> language and test them using the `test` function in `main.py`.