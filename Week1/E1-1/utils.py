offset_64 = 1<<63
mask_64 = (1<<64) -1
min_int64 = -(1<<63)
max_int64 = (1<<63)-1

def input_int() -> int:
    x = int(input())
    x = min(max_int64,max(min_int64,x))
    return x

def to_signed(x):
    return ((x + offset_64) & mask_64) - offset_64
    
def neg64(x): 
    return to_signed(-x)

def sub64(x,y): 
    return to_signed(x-y)

def add64(x,y):
    return to_signed(x+y)
