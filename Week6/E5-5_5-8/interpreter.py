import ast
from ast import *
from utils import *
import sys

class InterpLint:
    def interp_exp(self, e, env):
        match e:
            case BinOp(left, Add(), right):
                l = self.interp_exp(left, env)
                r = self.interp_exp(right, env) 
                return add64(l, r)
            case BinOp(left, Sub(), right):
                l = self.interp_exp(left, env) 
                r = self.interp_exp(right, env) 
                return sub64(l, r)
            case UnaryOp(USub(), v):return neg64(self.interp_exp(v, env))
            case Constant(value): return value
            case Call(Name('input_int'), []): return int(input())
            case _:raise Exception('error in interp_exp, unexpected:\n' + repr(e))

    def interp_stmt(self, s, env, cont):
        match s:
            case Expr(Call(Name('print'), [arg])): 
                val = self.interp_exp(arg, env) 
                sys.stdout.write(str(val))
                return self.interp_stmts(cont, env)
            case Expr(value): 
                self.interp_exp(value, env)
                return self.interp_stmts(cont, env)
            case _:raise Exception('error in interp_stmt, unexpected:\n' + dump(s, indent=2))

    def interp_stmts(self, ss, env): 
        match ss:
            case []: return 0
            case [s, *ss]: return self.interp_stmt(s, env, ss)

    def interp(self, p):
        match p:
            case Module(body): self.interp_stmts(body, {})

class InterpLvar(InterpLint): 
    def interp_exp(self, e, env):
        match e:
            case Name(id):
                return env[id]
            case _:
                return super().interp_exp(e, env)

    def interp_stmt(self, s, env, cont): 
        match s:
            case Assign([Name(id)], e):
                env[id] = self.interp_exp(e, env)
                return self.interp_stmts(cont, env)
            case _:
                return super().interp_stmt(s, env, cont)

class InterpLif(InterpLvar):
    def interp_exp(self, e, env):
        match e:
            case Begin(ss, e):
                self.interp_stmts(ss, env)
                return self.interp_exp(e, env)
            case IfExp(test, body, orelse):
                if self.interp_exp(test, env):
                    return self.interp_exp(body,env)
                else:
                    return self.interp_exp(orelse, env)
            case UnaryOp(Not(), v):
                return not self.interp_exp(v, env)
            case BoolOp(And(), values):
                if self.interp_exp(values[0], env):
                    return self.interp_exp(values[1], env)
                else:
                    return False
            case BoolOp(Or(), values):
                if not self.interp_exp(values[0], env):
                    return self.interp_exp(values[1], env)
                else:
                    return True
            case Compare(left, [cmp], [right]):
                l = self.interp_exp(left, env)
                r = self.interp_exp(right, env)
                return self.interp_cmp(cmp)(l, r)
            case _:
                return super().interp_exp(e, env)

    def interp_stmt(self, s, env, cont):
        match s:
            case If(test, body, orelse):
                if self.interp_exp(test, env):
                    return self.interp_stmts(body + cont, env)
                else:
                    return self.interp_stmts(orelse + cont, env)
            case _:
                return super().interp_stmt(s, env, cont)

    def interp_cmp(self, cmp):
        match cmp:
            case Lt():
                return lambda x,y: x < y
            case LtE():
                return lambda x,y: x <= y
            case Gt():
                return lambda x,y: x > y
            case GtE():
                return lambda x,y: x >= y
            case Eq():
                return lambda x,y: x == y
            case NotEq():
                return lambda x,y: x != y

class InterpCif(InterpLif):

  def interp(self, p):
    match p:
      case CProgram(blocks):
        env = {}
        self.blocks = blocks
        self.interp_stmts(blocks['start'], env)

  def interp_stmts(self, ss, env):
    match ss:
      case [t]:
        return self.interp_tail(t, env)
      case [s, *ss]:
        self.interp_stmt(s, env, [])
        return self.interp_stmts(ss, env)

  def interp_tail(self, s, env):
    match s:
      case Return(value):
        return self.interp_exp(value, env)
      case Goto(label):
        return self.interp_stmts(self.blocks[label], env)
      case If(test, [Goto(thn)], [Goto(els)]):
        match self.interp_exp(test, env):
          case True:
            return self.interp_stmts(self.blocks[thn], env)
          case False:
            return self.interp_stmts(self.blocks[els], env)
      case _:
        raise Exception('interp_tail: unexpected ' + repr(s))

def interp_Lint(p):
    return InterpLint().interp(p)

def interp_Lvar(p):
    return InterpLvar().interp(p)

def interp_Lif(p):
    return InterpLif().interp(p)

def interp_Cif(p):
    return InterpCif().interp(p)

