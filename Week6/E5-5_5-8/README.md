# Exercises E5.5 and E5.8 (Graded together)

Implement the remaining passes inside `compiler.py` to compile to
`x86<sub>Int</sub>` as described in Exercises 5.5 and 5.8 of the book (using
stack allocation instead of register allocation). Test your compiler on all the
tests you designed for `E5-4` and make sure the input-output behaviour of the
program is the same whether it runs on the `L<sub>if</sub>` interpreter or
natively on `x86`.

Use the provided script `run_tests.py` to test your compiler. We will use a
copy of the same script to grade your tests and compiler. If you find a bug or
encounter issues in `run_tests.py`, please share them (early) on the Discord
forum and we will update the template as necessary. See usage example below.

The expected output for a single file `tests/t0_and.py` with the default
template compiler is:
```
error: shrinked program should not contain a BoolOp such as 'and':
    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))

error: monadic program should not contain a complex expression as argument of a print:
    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))

error: Cif program should not contain a complex expression as argument of 'print' :
start:
    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))
    return 0


tests/t0_and.s: Assembler messages:
tests/t0_and.s: Warning: end of file not at end of a line; newline inserted
tests/t0_and.s:1: Error: no such instruction: `none'
sh: 1: ./a.out: not found
0a1
> 42
compiler If_Compiler, executable failed on test tests/t0_and
0/4 passes successful, test failed
```

One possible valid output for a single file `tests/t0_and.py` with a correct compiler and the `--trace` option is:
```
test: tests/t0_and.py

# source program: t0_and

    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))


Module([  Assign([Name('a')], Call(Name('input_int'), [])),   Expr(Call(Name('print'), [IfExp(Compare(Name('a'), [Gt()], [Constant(3)]) And() Compare(Name('a'), [Lt()], [Constant(5)]), Constant(42), Constant(24))]))])


# shrink

    a = input_int()
    print((42 if (a < 5 if a > 3 else False) else 24))


shrinked program does not contain 'and' and 'or' BoolOps
compiler If_Compiler success on pass shrink on test
tests/t0_and

shrinked program maintains the expected input-output behaviour


# remove_complex_operands

    a = input_int()
    _t1 = (42 if (a < 5 if a > 3 else False) else 24)
    print(_t1)

Module([  Assign([Name('a')], Call(Name('input_int'), [])),   Assign([Name('_t1')], IfExp(IfExp(Compare(Name('a'), [Gt()], [Constant(3)]), Compare(Name('a'), [Lt()], [Constant(5)]), Constant(False)), Constant(42), Constant(24))),   Expr(Call(Name('print'), [Name('_t1')]))])
monadic program contains the expected atomic expressions
compiler If_Compiler success on pass remove_complex_operands on test
tests/t0_and

monadic program maintains the expected input-output behaviour


# explicate_control

block.0:
    print(_t1)
    return 0

block.1:
    _t1 = 42
    goto block.0

block.2:
    _t1 = 24
    goto block.0

block.3:
    if a < 5:
      goto block.1
    else:
      goto block.2

start:
    a = input_int()
    if a > 3:
      goto block.3
    else:
      goto block.2


CProgram(body={'block.0': [  Expr(Call(Name('print'), [Name('_t1')])),   Return(Constant(0))], 'block.1': [  Assign([Name('_t1')], Constant(42)), Goto(label='block.0')], 'block.2': [  Assign([Name('_t1')], Constant(24)), Goto(label='block.0')], 'block.3': [If(Compare(Name('a'), [Lt()], [Constant(5)]), [Goto(label='block.1')], [Goto(label='block.2')])], 'start': [  Assign([Name('a')], Call(Name('input_int'), [])), If(Compare(Name('a'), [Gt()], [Constant(3)]), [Goto(label='block.3')], [Goto(label='block.2')])]})
cif program contains the expected statements and expressions
compiler If_Compiler success on pass explicate_control on test
tests/t0_and

cif program maintains the expected input-output behaviour


# select_instructions

	.align 16
start:
    callq read_int
    movq %rax, a
    movq $3, %rax
    cmpq %rax, a
    jg block.3
    jmp block.2

	.align 16
block.3:
    movq $5, %rax
    cmpq %rax, a
    jl block.1
    jmp block.2

	.align 16
block.2:
    movq $24, %rax
    movq %rax, _t1
    jmp block.0

	.align 16
block.1:
    movq $42, %rax
    movq %rax, _t1
    jmp block.0

	.align 16
block.0:
    movq _t1, %rdi
    callq print
    movq $0, %rax
    retq




# assign_homes

	.align 16
start:
    callq read_int
    movq %rax, -8(%rbp)
    movq $3, %rax
    cmpq %rax, -8(%rbp)
    jg block.3
    jmp block.2

	.align 16
block.3:
    movq $5, %rax
    cmpq %rax, -8(%rbp)
    jl block.1
    jmp block.2

	.align 16
block.2:
    movq $24, %rax
    movq %rax, -16(%rbp)
    jmp block.0

	.align 16
block.1:
    movq $42, %rax
    movq %rax, -16(%rbp)
    jmp block.0

	.align 16
block.0:
    movq -16(%rbp), %rdi
    callq print_int
    movq $0, %rax
    jmp conclusion

	.globl main
	.align 16
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $16, %rsp
    jmp start

	.align 16
conclusion:
    addq $16, %rsp
    popq %rbp
    retq




# prelude_and_conclusion

	.align 16
start:
    callq read_int
    movq %rax, -8(%rbp)
    movq $3, %rax
    cmpq %rax, -8(%rbp)
    jg block.3
    jmp block.2

	.align 16
block.3:
    movq $5, %rax
    cmpq %rax, -8(%rbp)
    jl block.1
    jmp block.2

	.align 16
block.2:
    movq $24, %rax
    movq %rax, -16(%rbp)
    jmp block.0

	.align 16
block.1:
    movq $42, %rax
    movq %rax, -16(%rbp)
    jmp block.0

	.align 16
block.0:
    movq -16(%rbp), %rdi
    callq print_int
    movq $0, %rax
    jmp conclusion

	.globl main
	.align 16
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $16, %rsp
    jmp start

	.align 16
conclusion:
    addq $16, %rsp
    popq %rbp
    retq




4/4 passes successful, test success
```
Note that in the example above, the `patch_instructions` and `prelude_and_conclusion` don't do anything, which is fine because
we do not use a register allocator. You may also follow the structure of the book and have more granularity, with work done 
by `patch_instructions` and `prelude_and_conclusion`.

The expected output for a directory `tests` containing two tests with a correct compiler (without the `--trace` option) is:
```
tests: 2/2 for compiler If_Compiler
passes: 8/8 for compiler If_Compiler
```

As for week 4 and 5, you may show intermediary outputs with `--trace` and stop on the first error with `--strict`.

# Common Mistakes and Solutions

`
Traceback (most recent call last):
  File "/home/template/E5-3/run_rco.py", line 7, in <module>
    from compiler import If_Compiler
ImportError: cannot import name 'If_Compiler' from 'compiler' (/home/template/E5-3/compiler.py)
`

The name of your compiler is not 'If\_Compiler' as expected. Modify it.
