from utils import *
from x86_ast import *

class If_Compiler:

    ############################
    # Shrink
    ############################
    def shrink(self, p: Module) -> Module:
        return p # no op

    def shrink_stmt(self, s):
        pass

    def shrink_exp(self, e):
        pass

    ############################
    # Remove Complex Operands
    ############################
    def remove_complex_operands(self, p: Module) -> Module:
        match p:
            case Module(body):
                return Module(self.rco_stmts(body))
            case _:
                raise Exception('error in remove_complex_operands, unexpected ' + repr(body))

    def rco_exp(self, e, need_atom):
        return e

    def rco_stmt(self, s):
        return [s]

    def rco_stmts(self, ss):
        ts = []
        for s in ss:
            ts.extend(self.rco_stmt(s))
        return ts

    ############################
    # Explicate Control
    ############################
    def explicate_control(self, p: Module) -> CProgram:
        match p:
            case Module(body):
                blocks = {}
                blocks["start"] = self.explicate_stmts(body, [ Return(Constant(0)) ], blocks)
                return CProgram(blocks)
            case _:
                raise Exception('If_Compiler error in explicate_control, invalid program ' + repr(p))

    def explicate_stmts(self, ss, cont, blocks):
        for s in reversed(ss):
            cont = self.explicate_stmt(s, cont, blocks)
        return cont

    # Feel free to structure the rest of your code according
    # to the book example or not, including 'explicate_stmt'
    def explicate_stmt(self, s, cont, blocks):
        return [s] + cont

    ############################
    # Select Instructions
    ############################

    def select_instructions(self, p: CProgram) -> X86Program:
        pass

    ############################
    # Assign Homes
    ############################

    def assign_homes(self, p: X86Program) -> X86Program:
        pass

    ############################
    # Patch Instructions
    ############################

    def patch_instructions(self, p: X86Program) -> X86Program:
        pass

    ############################
    # Prelude and Conclusion
    ############################

    def prelude_and_conclusion(self, p: X86Program) -> X86Program:
        pass

