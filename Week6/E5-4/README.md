# Exercise 5.4: Explicate Control Pass for Lif

Implement the `explicate_control` pass inside `compiler.py` (as well as all the previous passes from Week5 exercises). Provide 12 tests under `tests/t{team number}_{short description}.py` as well as corresponding inputs in `tests/t{team number}_{short description}.in` and expected output `tests/t{team number}_{short description}.golden`. Design your tests to exercise different cases of your compiler, not covered in the textbook and devised independently of other teams, and ensure they can distinguish between correct and incorrect behaviour through differing outputs.

Use the following script to run the 'explicate\_control' pass: 
```
    python3 run_exp_ctl.py <file_or_directory>
```
The expected output for a single file `tests/t0_and.py` with the default template compiler is:
```
error: shrinked program should not contain a BoolOp such as 'and':
    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))

error: monadic program should not contain a complex expression as argument of a print:
    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))

error: Cif program should not contain a complex expression as argument of 'print' :
start:
    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))
    return 0


0/1 Cif passes successful
```

The expected output for a single file `tests/t0_and.py` with a correct compiler and the `--trace` option is:
```
test: tests/t0_and.py

# source program: t0_and

    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))


Module([  Assign([Name('a')], Call(Name('input_int'), [])),   Expr(Call(Name('print'), [IfExp(Compare(Name('a'), [Gt()], [Constant(3)]) And() Compare(Name('a'), [Lt()], [Constant(5)]), Constant(42), Constant(24))]))])


# shrink

    a = input_int()
    print((42 if (a < 5 if a > 3 else False) else 24))


shrinked program does not contain 'and' and 'or' BoolOps
compiler If_Compiler success on pass shrink on test
tests/t0_and

shrinked program maintains the expected input-output behaviour


# remove_complex_operands

    a = input_int()
    _t1 = (42 if (a < 5 if a > 3 else False) else 24)
    print(_t1)

Module([  Assign([Name('a')], Call(Name('input_int'), [])),   Assign([Name('_t1')], IfExp(IfExp(Compare(Name('a'), [Gt()], [Constant(3)]), Compare(Name('a'), [Lt()], [Constant(5)]), Constant(False)), Constant(42), Constant(24))),   Expr(Call(Name('print'), [Name('_t1')]))])
monadic program contains the expected atomic expressions
compiler If_Compiler success on pass remove_complex_operands on test
tests/t0_and

monadic program maintains the expected input-output behaviour


# explicate_control

block.0:
    print(_t1)
    return 0

block.1:
    _t1 = 42
    goto block.0

block.2:
    _t1 = 24
    goto block.0

block.3:
    if a < 5:
      goto block.1
    else:
      goto block.2

start:
    a = input_int()
    if a > 3:
      goto block.3
    else:
      goto block.2


CProgram(body={'block.0': [  Expr(Call(Name('print'), [Name('_t1')])),   Return(Constant(0))], 'block.1': [  Assign([Name('_t1')], Constant(42)), Goto(label='block.0')], 'block.2': [  Assign([Name('_t1')], Constant(24)), Goto(label='block.0')], 'block.3': [If(Compare(Name('a'), [Lt()], [Constant(5)]), [Goto(label='block.1')], [Goto(label='block.2')])], 'start': [  Assign([Name('a')], Call(Name('input_int'), [])), If(Compare(Name('a'), [Gt()], [Constant(3)]), [Goto(label='block.3')], [Goto(label='block.2')])]})
cif program contains the expected statements and expressions
compiler If_Compiler success on pass explicate_control on test
tests/t0_and

cif program maintains the expected input-output behaviour

1/1 Cif passes successful
```

The expected output for a directory `tests` containing two tests with a correct compiler (without the `--trace` option) is:
```
Final results:
  successful Cif passes over 2 tests: 2/2 for compiler If_Compiler
```

As for week 4 and 5, you may show intermediary outputs with `--trace` and stop on the first error with `--strict`.

# Common Mistakes and Solutions

`
Traceback (most recent call last):
  File "/home/template/E5-3/run_rco.py", line 7, in <module>
    from compiler import If_Compiler
ImportError: cannot import name 'If_Compiler' from 'compiler' (/home/template/E5-3/compiler.py)
`

The name of your compiler is not 'If\_Compiler' as expected. Modify it.
