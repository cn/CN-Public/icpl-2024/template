# repository template

This template provides the conventions for organizing code and tests in your team's repository. Each week, we will create a new folder for each exercise, where you can find all the relevant conventions for that exercise.

Add the template repository to the remotes of your repository:

````
    git remote add template ssh://git@git.scicore.unibas.ch:2222/cn/CN-Public/icpl-2024/template.git
````

You will later be able to pull the latest changes with:

````
    git pull template main
````

