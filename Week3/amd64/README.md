
In the following two exercises, you'll explore Assembly language x86<sub>Int</sub> that you learned in the lectures! You'll use basic assembly instructions and discover the function call mechanism in Assembly language.

Since Assembly language is platform-dependent, we need a convention for synchronization. By choosing AT&T syntax for our assembly language, we aim to set up a 64-bit Ubuntu environment with the GNU assembler. If your hardware is based on the x86 architecture, you can install a hypervisor such as VMware, VirtualBox, or UTM to run a virtual machine. For newer MacBooks with ARM architecture, you will need to install a hypervisor and run your Ubuntu virtual machine within an emulator.

# Exercise amd64.1
In this exercise, your task is to write a function called *linear_fun* which calculates `-3x + 5` **in Assembly language**. The input *x* will be provided by the user through the function `read_int`. This function, along with another function called `print_int`, is implemented in C, and you can invoke them in your assembly code just like regular assembly procedure calls. The file *runtime.c* is as follows:

```C
#include <stdio.h>

typedef long long int64;

void print_int(int64 value) {
    printf("%lld\n", value);
}

int64 read_int() {
    int64 value;
    scanf("%lld", &value);
    return value;
}
```
After reading the input *x*, you must calculate `-3x + 5` by calling your implementation of the function `linear_fun` and then print the result using the function `print_int`. To output a string to the standard output, the function `put_str` is implemented in Assembly. To guide the end user of your program about the calculated value, you should call this function with the proper message. The file name for your implementation must be `amd64_1.asm` with the following structure:

```Python
.data
imsg: .string "Enter your number:"
omsg: .string "The result is:"

.text
.global main

main:
    ############ Prelude
    pushq %rbp
    movq %rsp, %rbp

    ############ Showing the message 'Enter your number:'
    movq $18, %rdi
    leaq imsg(%rip), %rsi
    callq put_str

    ############ Reading an integer from the user
    callq read_int
    
    ############ Calculating our desired function on the user input
    movq %rax, %rdi
    callq linear_func
    pushq %rax
    
    ############ Showing the message 'The result is:'
    movq $14, %rdi
    leaq omsg(%rip), %rsi
    callq put_str
    
    ############ Printing the calculated integer output    
    popq %rdi           
    callq print_int      

    ############ Exiting the program and returning 0 to the OS
    movq $60, %rax           
    xorq %rdi, %rdi          
    syscall                  


    ############ Implementation of the function put_str
put_str:
    pushq %rbp
    movq %rsp, %rbp
    movq %rdi, %rdx
    movq $1, %rax
    movq $1, %rdi
    syscall
    popq %rbp
    retq
    
linear_func:
    # TBD: your implementation for linear_func

```
To compile your code use the following commands:

```
gcc -c runtime.c -o runtime.o
as amd64_1.asm -o amd64_1.o
gcc -o myprog amd64_1.o runtime.o

```

The first command compiles the file `runtime.c` and generates the object file `runtime.o`. The second line does the same thing for the `amd64_1.asm` and finally, the last command links both object files and genrates a binary executable `myprog` file.

Whith the following commands you can run your compiled program:
```
chmod +x myprog
./myprog
```

# Exercise amd64.2

In this exercise, you will work with local variables, which, as discussed in the lecture, reside on the stack. Your task is to reimplement the following C code in our x86<sub>Int</sub> language, then compile and run it!

```C
#include <stdio.h>

typedef long long int64;

int64 main() {
    int64 x,y,z;
    scanf("%lld", &x);
    scanf("%lld", &y);
    z = 2*x-y+1;
    printf("%lld",z);
    return 0;
}
```
To read from input and print on the screen you must call the functions 'read_int' and 'print_int' functions written in the file *runtime.c*.

`Tip I`: Don't do any optimization and use local variables as the C code does.

`Tip II` : Number of bytes of local variables must be divisible by 16.