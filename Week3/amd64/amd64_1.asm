.data
imsg: .string "Enter your number:"
omsg: .string "The result is:"

.text
.global main

main:
    ############ Prelude
    pushq %rbp
    movq %rsp, %rbp

    ############ Showing the message 'Enter your number:'
    movq $18, %rdi
    leaq imsg(%rip), %rsi
    callq put_str

    ############ Reading an integer from the user
    callq read_int
    
    ############ Calculating our desired function on the user input
    movq %rax, %rdi
    callq linear_func
    pushq %rax
    
    ############ Showing the message 'The result is:'
    movq $14, %rdi
    leaq omsg(%rip), %rsi
    callq put_str
    
    ############ Printing the calculated integer output    
    popq %rdi           
    callq print_int      

    ############ Exiting the program and returning 0 to the OS
    movq $60, %rax           
    xorq %rdi, %rdi          
    syscall                  


    ############ Implementation of the function put_str
put_str:
    pushq %rbp
    movq %rsp, %rbp
    movq %rdi, %rdx
    movq $1, %rax
    movq $1, %rdi
    syscall
    popq %rbp
    retq
    
linear_func:
    # TBD: your implementation for linear_func
