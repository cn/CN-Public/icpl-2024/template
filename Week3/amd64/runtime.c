#include <stdio.h>

typedef long long int64;

void print_int(int64 value) {
    printf("%lld\n", value);
}

int64 read_int() {
    int64 value;
    scanf("%lld", &value);
    return value;
}
