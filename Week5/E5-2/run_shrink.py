import sys
import os
import traceback
from interpreter import interp_Lif
import argparse
from utils import *
from compiler import If_Compiler

class Shrinked:

    def is_shrinked_exp(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case Call(Name('input_int'), []): return True
            case BoolOp(op, [lhs, rhs]): # No need to recurse because it will always be false nonetheless
                raise Exception("error: shrinked program should not contain a BoolOp such as '" + str(op) + "':")
           
            case Call(Name('print'), [e]): return self.is_shrinked_exp(e)
            case UnaryOp(op, e): return self.is_shrinked_exp(e)
            case BinOp(lhs, op, rhs):
                return self.is_shrinked_exp(lhs) and self.is_shrinked_exp(rhs)
            case Compare(lhs, [cmp], [rhs]):
                return self.is_shrinked_exp(lhs) and self.is_shrinked_exp(rhs)
            case IfExp(cond, thn, els):
                return self.is_shrinked_exp(cond) and \
                       self.is_shrinked_exp(thn) and \
                       self.is_shrinked_exp(els)
            case _:
                raise Exception("Shrinked: Unsupported expression " + repr(e))

    def is_shrinked_stmt(self, s):
        match s:
            case Expr(e):
                return self.is_shrinked_exp(e)
            case Assign([Name(var)], e):
                return self.is_shrinked_exp(e)
            case If(e, thn, els):
                return self.is_shrinked_exp(e) and \
                       self.are_shrinked_stmts(thn) and \
                       self.are_shrinked_stmts(els)
            case _:
                raise Exception("Shrinked: Unsupported statement" + repr(s))

    def are_shrinked_stmts(self, ss):
        for s in ss:
            if not self.is_shrinked_stmt(s):
                return False

        return True

    def is_shrinked(self, p):
        match p:
            case Module(ss):
                return self.are_shrinked_stmts(ss)
            case _:
                raise Exception('error in is_shrinked, invalid program ' + repr(p))

def is_shrinked(p):
    return Shrinked().is_shrinked(p)

def is_python_extension(filename):
    s = os.path.splitext(filename)
    if len(s) > 1:
        return s[1] == ".py"
    else:
        return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Command-line tester for compiler')
    parser.add_argument('path', type=str, help='Python program for a single test or directory of tests')
    parser.add_argument('--trace', action='store_true', help='Show intermediary representations and test results')
    parser.add_argument('--strict', action='store_true', help='Stop on first pass test failure', default=False)
    args = parser.parse_args()

    if args.trace:
        enable_tracing()

    interp_dict = {
        'shrink': interp_Lif,
    }

    type_checker_dict = {
        'shrink': is_shrinked,
    }

    if os.path.isfile(args.path):
        (succ_passes, nb_passes, succ_test) = run_one_test(args.path, If_Compiler(), 'If_Compiler', type_checker_dict, interp_dict, stop_on_failed=args.strict)
        print("{}/{} passes successful".format(
            succ_passes, 
            nb_passes))
    elif os.path.isdir(args.path):
        run_tests(args.path, If_Compiler(), 'If_Compiler', type_checker_dict, interp_dict, stop_on_failed=args.strict)
    else:
        sys.stderr.write("Invalid path '" + args.path + "', neither a file or directory")
        sys.exit(1)

