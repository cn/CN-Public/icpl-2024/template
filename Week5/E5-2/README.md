# Exercise 5.2: Shrink Pass for Lif

Implement the shrink pass inside `compiler.py`. Provide four tests under `tests/t{team number}_{short description}.py` as well as corresponding inputs in `tests/t{team number}_{short description}.in` and expected output `tests/t{team number}_{short description}.golden`. Design your tests to exercise different cases of your compiler, not covered in the textbook and devised independently of other teams, and ensure they can distinguish between correct and incorrect behaviour through differing outputs.

Use the following script to run the shrink pass: 
```
    python3 run_shrink.py <file_or_directory>
```

The expected output for a single file `tests/correct/t0_print_int.py` is:
```
test: tests/t0_print_int.py
shrinked program does not contain 'and' and 'or' BoolOps
shrinked program maintained the expected input-output behaviour

1/1 passes successful
```

The expected output for a directory `tests` containing two tests is:
```
test: tests/t0_and.py
error: shrinked program should not contain a BoolOp such as 'and':
    a = input_int()
    print((42 if (a > 3 and a < 5) else 24))


test: tests/t0_print_int.py
shrinked program does not contain 'and' and 'or' BoolOps
shrinked program maintained the expected input-output behaviour


Final results:
  successful passes: 1/2 for compiler If
```

As for week 4, you may show intermediary outputs with `--trace` and stop on the first error with `--strict`.
