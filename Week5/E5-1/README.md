# Exercise 5.1: Type Checker for Lif

Read the type checker code (`type_check_Lvar.py` and `type_check_Lif.py`). Provide original tests, i.e. not found in the textbook and devised independently of other teams, that exercise different language features of `Lif`:

1. 5 examples, under `tests/correct/t{team_number}_{short_description}.py`, that correctly type check.
2. 5 examples, under `tests/incorrect/t{team_number}_{short_description}.py`, that result in type errors.

Use the following script to run the type checker: 
```
    python3 run_type_checker.py <file_or_directory>
```

The expected output for a single file `tests/correct/t0_print_int.py` is:
```
    tests/correct/t0_print_int.py: valid

```

The expected output for a directory `tests` containing two sub-directories 'correct' and 'incorrect' is:
```
    tests/correct/t0_print_int.py: valid
    tests/incorrect/t0_not_add.py: valid

```
