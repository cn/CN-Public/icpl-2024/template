from utils import *
from x86_ast import *

class If_Compiler:

    def shrink(self, p):
        return p # no op

    def shrink_stmt(self, s):
        pass

    def shrink_exp(self, e):
        pass

    def remove_complex_operands(self, p):
        match p:
            case Module(body):
                return Module(self.rco_stmts(body))
            case _:
                raise Exception('error in remove_complex_operands, unexpected ' + repr(body))

    def rco_exp(self, e, need_atom):
        return e

    def rco_stmt(self, s):
        return [s]

    def rco_stmts(self, ss):
        ts = []
        for s in ss:
            ts.extend(self.rco_stmt(s))
        return ts



