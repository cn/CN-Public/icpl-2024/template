# Exercise 5.3: Remove Complex Operand Pass for Lif

Implement the `remove_complex_operand` pass inside `compiler.py` (and the 'shrink' pass from E5.2). Provide four tests under `tests/t{team number}_{short description}.py` as well as corresponding inputs in `tests/t{team number}_{short description}.in` and expected output `tests/t{team number}_{short description}.golden`. Design your tests to exercise different cases of your compiler, not covered in the textbook and devised independently of other teams, and ensure they can distinguish between correct and incorrect behaviour through differing outputs.

Use the following script to run the 'remove_complex_operand' pass: 
```
    python3 run_rco.py <file_or_directory>
```

The expected output for a single file `tests/t0_and.py` with a correct compiler is:
```
test: tests/t0_and.py
shrinked program does not contain 'and' and 'or' BoolOps
shrinked program maintained the expected input-output behaviour
monadic program contains the expected atomic expressions
monadic program maintains the expected input-output behaviour

2/2 passes successful
```

The expected output for a directory `tests` containing two tests with a correct compiler is:
```
test: tests/t0_and.py
shrinked program does not contain 'and' and 'or' BoolOps
shrinked program maintains the expected input-output behaviour
monadic program contains the expected atomic expressions
monadic program maintains the expected input-output behaviour

test: tests/t0_print_int.py
shrinked program does not contain 'and' and 'or' BoolOps
shrinked program maintains the expected input-output behaviour
monadic program contains the expected atomic expressions
monadic program maintains the expected input-output behaviour


Final results:
  successful passes: 4/4 for compiler If_Compiler
```

As for week 4 and exercise E5.2, you may show intermediary outputs with `--trace` and stop on the first error with `--strict`.

# Common Mistakes and Solutions

`
Traceback (most recent call last):
  File "/home/template/E5-3/run_rco.py", line 7, in <module>
    from compiler import If_Compiler
ImportError: cannot import name 'If_Compiler' from 'compiler' (/home/template/E5-3/compiler.py)
`

The name of your compiler is not 'If\_Compiler' as expected. Modify it.
