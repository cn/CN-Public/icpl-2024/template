import ast
from ast import*
from compiler import Compiler
from interpreter import interp_Lvar
from unparse import unparse

def test(src_code):
    prg1 = ast.parse(src_code)
    LvarMon_p = Compiler().remove_complex_operands(prg1)
    print('remove_complex_operands output:')
    print(unparse(LvarMon_p))
    print('Interpreted as:')
    interp_Lvar(LvarMon_p)

test('x=42')