from ast import*
from typing import List, Tuple,Dict
from utils import generate_name

Binding = Tuple[Name, expr]
Temporaries = List[Binding]
_DV_ = 'DUMMY_VARIABLE'

class Compiler:
    def needs_atomic(self,x):
        pass #TBD

    def is_atom(self, x):
        pass #TBD

    def rco_exp(self, e: expr, need_atomic : bool) -> Tuple[expr, Temporaries]:
        pass #TBD

    def rco_stmt(self, s: stmt) -> List[stmt]:
        pass #TBD

    def remove_complex_operands(self, p: Module) -> Module:
        match p:
            case Module(body):
                lst = []
                for stmt in body:
                    if not self.needs_atomic(stmt):lst.append(stmt)
                    else:
                        s = self.rco_stmt(stmt)
                        lst.extend(s)
                return Module(lst)
            case _:raise Exception('error in remove_complex_operands, unexpected ' + repr(body))