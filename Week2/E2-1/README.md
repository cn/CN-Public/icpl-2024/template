# Exercise 2.1

In this exercise, your task is to implement a compiler from L<sub>Var</sub> to L<sub>Var</sub><sup>mon</sup> as described in Exercise 2.1 of the book. The grammars for both languages are available in the slides and reference book for your compatibility checks. To evaluate your compiled programs, you will also need the L<sub>Var</sub> interpreter, which can be found in the book or the slides from the last session.

In the file `main.py`, a function called `test` is defined, which takes source code for programs written in L<sub>Var</sub> language, compiles it to L<sub>Var</sub><sup>mon</sup>, displays the concrete syntax of the compiled program, and finally interprets it. Your tasks are as follows:

1. **Complete the L<sub>Var</sub> Interpreter**: Implement the source code for the **InterpLvar** class in the file `interpreter.py`. You can find the necessary code in the book or the slides. This part is essentially a copy-pasting task.

2. **Write the Compiler**: Complete all the functions defined in the class **Compiler** in the file `compiler.py` so that the function *remove_complex_operands* compiles from L<sub>Var</sub> to L<sub>Var</sub><sup>mon</sup>.

3. **Provide 3 Test Cases**: Create **three simple but non-trivial programs** in the L<sub>Var</sub> language and compile them to L<sub>Var</sub><sup>mon</sup> using the `test` function in `main.py`.