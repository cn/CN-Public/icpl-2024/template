import ast
from ast import *

def unparse(node):
    match node:
        case ast.Constant(value):
            return str(value)
        case ast.Name(var):
            return var
        case ast.Call(Name('input_int'),[]):
            return 'input_int()'
        case ast.UnaryOp(USub(),e):
            return '-' + unparse(e)
        case ast.BinOp(e1,Add(),e2):
            return unparse(e1) + '+' + unparse(e2)
        case ast.BinOp(e1,Sub(),e2):
            return unparse(e1) + '-' + unparse(e2)
        case ast.Expr(Call(Name('print'),[arg])):
            return f"print({unparse(arg)})"
        case ast.Expr(value):
            return unparse(value)
        case ast.Assign([Name(var)],e):
            return var + '=' + unparse(e)
        case ast.Module(body):
            ret = ''
            for s in body:
                ret += unparse(s) + '\n'
            return ret.strip()
        case _:
            raise Exception('error in unparse, unexpected ' + repr(node)) 