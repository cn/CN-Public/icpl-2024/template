from sys import platform

offset_64 = 1<<63
mask_64 = (1<<64) -1
min_int64 = -(1<<63)
max_int64 = (1<<63)-1
name_id = 0
indent_amount = 2

def input_int() -> int:
    x = int(input())
    x = min(max_int64,max(min_int64,x))
    return x

def to_signed(x):
    return ((x + offset_64) & mask_64) - offset_64
    
def neg64(x): 
    return to_signed(-x)

def sub64(x,y): 
    return to_signed(x-y)

def add64(x,y):
    return to_signed(x+y)

def generate_name(name):
    global name_id
    ls = name.split('_')
    new_id = name_id
    name_id += 1
    return ls[0] + '_' + str(new_id)

def label_name(n: str) -> str:
    if platform == "darwin": return '_' + n
    else:return n

def indent_stmt():
    return " " * indent_amount

def indent():
    global indent_amount
    indent_amount += 2

def dedent():
    global indent_amount
    indent_amount -= 2