# Exercise 8.2: Remove Complex Operand Pass for LFun

Implement the `shrink`, `reveal_functions`, and `remove_complex_operand` passes inside `compiler.py`. Provide six tests under `tests/t{team number}_{short description}.py` as well as corresponding inputs in `tests/t{team number}_{short description}.in` and expected output `tests/t{team number}_{short description}.golden`. Design your tests to exercise different cases of your compiler, not covered in the textbook and devised independently of other teams, and ensure they can distinguish between correct and incorrect behaviour through differing outputs.

Use the following script to run the tests:
```
    python3 run_tests.py <file_or_directory>
```

The expected output for a single file `tests/t0_print_f_input_int.py` with the default compiler is:
```
Shrinked: Top-level statements should only be function definitions
    def f() -> int:
      return (input_int() + 36)

    a = f
    print(a())

Revealed: Invalid Name(f), should be a FuncRef
    def f() -> int:
      return (input_int() + 36)

    a = f
    print(a())

Monadic: program should not contain a complex expression as parameter of a return stmt:
    def f() -> int:
      return (input_int() + 36)

    a = f
    print(a())

0/3 passes successful
```

The expected output for a single file `tests/t0_print_f_input_int.py` with a correct compiler is:
```
3/3 passes successful
```

One valid output for a directory `tests` containing one test with a correct compiler is:
```
test: tests/t0_print_f_input_int.py

# source program: t0_print_f_input_int

    def f() -> int:
      return (input_int() + 36)

    a = f
    print(a())


Module([FunctionDef(f,<ast.arguments object at 0x7f36d8612710>,[  Return(BinOp(Call(Name('input_int'), []), Add(), Constant(36)))]),   Assign([Name('a')], Name('f')),   Expr(Call(Name('print'), [Call(Name('a'), [])]))])


# type checking source program


# shrink

    def f() -> int:
      return (input_int() + 36)

    def main() -> int:
      a = f
      print(a())
      return 0



shrinked program does not contain top-level statements
compiler Fun_Compiler success on pass shrink on test
tests/t0_print_f_input_int

shrinked program maintains the expected input-output behaviour


# reveal_functions


functions were correctly revealed
compiler Fun_Compiler success on pass reveal_functions on test
tests/t0_print_f_input_int

function-revealed program maintains the expected input-output behaviour


# remove_complex_operands

    def f() -> int:
      tmp.0 = input_int()
      tmp.1 = (tmp.0 + 36)
      return tmp.1

    def main() -> int:
      a = {f}
      tmp.2 = a()
      print(tmp.2)
      return 0


Module([FunctionDef(f,[],[  Assign([Name('tmp.0')], Call(Name('input_int'), [])),   Assign([Name('tmp.1')], BinOp(Name('tmp.0'), Add(), Constant(36))),   Return(Name('tmp.1'))]), FunctionDef(main,<ast.arguments object at 0x7f36d86139d0>,[  Assign([Name('a')], FunRef(name='f', arity=0)),   Assign([Name('tmp.2')], Call(Name('a'), [])),   Expr(Call(Name('print'), [Name('tmp.2')])),   Return(Constant(0))])])
monadic program contains the expected atomic expressions
compiler Fun_Compiler success on pass remove_complex_operands on test
tests/t0_print_f_input_int

monadic program maintains the expected input-output behaviour



Final results:
  successful passes: 3/3 for compiler Fun_Compiler
```

As for previous weeks, you may show intermediary outputs with `--trace` and stop on the first error with `--strict`.
