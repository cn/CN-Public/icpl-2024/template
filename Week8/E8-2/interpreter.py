import ast
from ast import *
from utils import *
import sys

class InterpLint:
    def interp_exp(self, e, env):
        match e:
            case BinOp(left, Add(), right):
                l = self.interp_exp(left, env)
                r = self.interp_exp(right, env) 
                return add64(l, r)
            case BinOp(left, Sub(), right):
                l = self.interp_exp(left, env) 
                r = self.interp_exp(right, env) 
                return sub64(l, r)
            case UnaryOp(USub(), v):return neg64(self.interp_exp(v, env))
            case Constant(value): return value
            case Call(Name('input_int'), []): return int(input())
            case _:raise Exception('error in interp_exp, unexpected:\n' + repr(e))

    def interp_stmt(self, s, env, cont):
        match s:
            case Expr(Call(Name('print'), [arg])): 
                val = self.interp_exp(arg, env) 
                sys.stdout.write(str(val))
                return self.interp_stmts(cont, env)
            case Expr(value): 
                self.interp_exp(value, env)
                return self.interp_stmts(cont, env)
            case _:raise Exception('error in interp_stmt, unexpected:\n' + dump(s, indent=2))

    def interp_stmts(self, ss, env): 
        match ss:
            case []: return 0
            case [s, *ss]: return self.interp_stmt(s, env, ss)

    def interp(self, p):
        match p:
            case Module(body): self.interp_stmts(body, {})

class InterpLvar(InterpLint): 
    def interp_exp(self, e, env):
        match e:
            case Name(id):
                return env[id]
            case _:
                return super().interp_exp(e, env)

    def interp_stmt(self, s, env, cont): 
        match s:
            case Assign([Name(id)], e):
                env[id] = self.interp_exp(e, env)
                return self.interp_stmts(cont, env)
            case _:
                return super().interp_stmt(s, env, cont)

class InterpLif(InterpLvar):
    def interp_exp(self, e, env):
        match e:
            case Begin(ss, e):
                self.interp_stmts(ss, env)
                return self.interp_exp(e, env)
            case IfExp(test, body, orelse):
                if self.interp_exp(test, env):
                    return self.interp_exp(body,env)
                else:
                    return self.interp_exp(orelse, env)
            case UnaryOp(Not(), v):
                return not self.interp_exp(v, env)
            case BoolOp(And(), values):
                if self.interp_exp(values[0], env):
                    return self.interp_exp(values[1], env)
                else:
                    return False
            case BoolOp(Or(), values):
                if not self.interp_exp(values[0], env):
                    return self.interp_exp(values[1], env)
                else:
                    return True
            case Compare(left, [cmp], [right]):
                l = self.interp_exp(left, env)
                r = self.interp_exp(right, env)
                return self.interp_cmp(cmp)(l, r)
            case _:
                return super().interp_exp(e, env)

    def interp_stmt(self, s, env, cont):
        match s:
            case If(test, body, orelse):
                if self.interp_exp(test, env):
                    return self.interp_stmts(body + cont, env)
                else:
                    return self.interp_stmts(orelse + cont, env)
            case _:
                return super().interp_stmt(s, env, cont)

    def interp_cmp(self, cmp):
        match cmp:
            case Lt():
                return lambda x,y: x < y
            case LtE():
                return lambda x,y: x <= y
            case Gt():
                return lambda x,y: x > y
            case GtE():
                return lambda x,y: x >= y
            case Eq():
                return lambda x,y: x == y
            case NotEq():
                return lambda x,y: x != y

class InterpLwhile(InterpLif):

  def interp_stmt(self, s, env, cont):
    match s:
      case While(test, body, []):
        if self.interp_exp(test, env):
          return self.interp_stmts(body + [s] + cont, env)
        else:
          return self.interp_stmts(cont, env)
      case _:
        return super().interp_stmt(s, env, cont)

class InterpLtup(InterpLwhile):

  def interp_cmp(self, cmp):
    match cmp:
      case Is():
        return lambda x, y: x is y
      case _:
        return super().interp_cmp(cmp)      
    
  def interp_exp(self, e, env):
    match e:
      case Tuple(es, Load()):
        # use a list for mutability
        return [self.interp_exp(e, env) for e in es]
      case Subscript(tup, index, Load()):
        t = self.interp_exp(tup, env)
        n = self.interp_exp(index, env)
        return t[n]
      case Call(Name('len'), [tup]):
        t = self.interp_exp(tup, env)
        return len(t)
      case Allocate(length, typ):
        array = [None] * length
        return array
      case GlobalValue(name):
        return 0 # ???
      case _:
        return super().interp_exp(e, env)

  def interp_stmt(self, s, env, cont):
    match s:
      case Collect(size):
        return self.interp_stmts(cont, env)
      case Assign([Subscript(tup, index)], value):
        tup = self.interp_exp(tup, env)
        index = self.interp_exp(index, env)
        tup[index] = self.interp_exp(value, env)
        return self.interp_stmts(cont, env)
      case _:
        return super().interp_stmt(s, env, cont)

class InterpLarray(InterpLtup):

  def interp_exp(self, e, env):
    match e:
      case ast.List(es, Load()):
        return [self.interp_exp(e, env) for e in es]
      case BinOp(left, Mult(), right):
          l = self.interp_exp(left, env); r = self.interp_exp(right, env)
          return mul64(l, r)
      case Subscript(tup, index, Load()):
        t = self.interp_exp(tup, env)
        n = self.interp_exp(index, env)
        if n < len(t):
          return t[n]
        else:
          raise TrappedError('array index out of bounds')
      case AllocateArray(length, typ):
        array = [None] * length
        return array
      case Call(Name('array_len'), [tup]):
        t = self.interp_exp(tup, env)
        return len(t)
      case Call(Name('array_load'), [tup, index]):
        t = self.interp_exp(tup, env)
        n = self.interp_exp(index, env)
        if n < len(t):
          return t[n]
        else:
          raise TrappedError('array index out of bounds')
      case Call(Name('array_store'), [tup, index, value]):
        t = self.interp_exp(tup, env)
        n = self.interp_exp(index, env)
        if n < len(t):
          t[n] = self.interp_exp(value, env)
        else:
          raise TrappedError('array index out of bounds')
        return None
      case _:
        return super().interp_exp(e, env)

  def interp_stmt(self, s, env, cont):
    match s:
      case Assign([Subscript(tup, index)], value):
        t = self.interp_exp(tup, env)
        n = self.interp_exp(index, env)
        if n < len(t):
          t[n] = self.interp_exp(value, env)
        else:
          raise TrappedError('array index out of bounds')
        return self.interp_stmts(cont, env)
      case _:
        return super().interp_stmt(s, env, cont)

class Function:
    __match_args__ = ("name", "params", "body", "env")
    def __init__(self, name, params, body, env):
        self.name = name
        self.params = params
        self.body = body
        self.env = env
    def __repr__(self):
        return 'Function(' + self.name + ', ...)'

class InterpLfun(InterpLarray):

  def apply_fun(self, fun, args, e):
      match fun:
        case Function(name, xs, body, env):
          new_env = {x: v for (x,v) in env.items()}
          for (x,arg) in zip(xs, args):
              new_env[x] = arg
          return self.interp_stmts(body, new_env)
        case _:
          raise Exception('apply_fun: unexpected: ' + repr(fun))
    
  def interp_exp(self, e, env):
    match e:
      case Call(Name(f), args) if f in builtin_functions:
        return super().interp_exp(e, env)      
      case Call(func, args):
        f = self.interp_exp(func, env)
        vs = [self.interp_exp(arg, env) for arg in args]
        return self.apply_fun(f, vs, e)
      case FunRef(id, arity):
        return env[id]
      case _:
        return super().interp_exp(e, env)

  def interp_stmt(self, s, env, cont):
    match s:
      case Return(value):
        return self.interp_exp(value, env)
      case FunctionDef(name, params, bod, dl, returns, comment):
        if isinstance(params, ast.arguments):
            ps = [p.arg for p in params.args]
        else:
            ps = [x for (x,t) in params]
        env[name] = Function(name, ps, bod, env)
        return self.interp_stmts(cont, env)
      case _:
        return super().interp_stmt(s, env, cont)

  def interp(self, p):
    match p:
      case Module(ss):
        env = {}
        self.interp_stmts(ss, env)
        if 'main' in env.keys():
            self.apply_fun(env['main'], [], None)
      case _:
        raise Exception('interp: unexpected ' + repr(p))

def interp_Lint(p):
    return InterpLint().interp(p)

def interp_Lvar(p):
    return InterpLvar().interp(p)

def interp_Lif(p):
    return InterpLif().interp(p)

def interp_Lfun(p):
    return InterpLfun().interp(p)


