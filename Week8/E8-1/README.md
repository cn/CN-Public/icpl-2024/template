# Exercise 8.1: Type Checker for Lfun

Read the type checker code (`type_check_Lfun.py` and dependencies). Provide original tests, i.e. not found in the textbook and devised independently of other teams, that exercise different language features of `Lfun`:

1. 3 examples, under `tests/correct/t{team_number}_{short_description}.py`, that correctly type check.
2. 3 examples, under `tests/incorrect/t{team_number}_{short_description}.py`, that result in type errors *because of incorrect, inconsistent, or missing types*.

where `{team_number}` is a number from 1 to 6 (`0` is reserved for Ali and Erick).

Use the following script to run the type checker: 
```
    python3 run_type_checker.py <file_or_directory>
```

The expected output for a single file `tests/correct/t0_print_f_int.py` is:
```
    tests/correct/t0_print_f_int.py: valid

```

The default expected output for a directory `tests` containing two sub-directories 'correct' and 'incorrect' is:
```
    tests/correct/t0_print_f_int.py: valid
    tests/incorrect/t0_7_params_fun.py: invalid, should have raised an exception
```

You must modify `type_check_Lfun.py` so that it correctly raises an exception for `tests/incorrect/t0_7_params_fun.py`. You should not modify any other file.

If you find a bug in `run_type_checker.py` or other support files, notify us on Discord.
