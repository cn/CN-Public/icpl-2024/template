import sys
import os
import traceback
from type_check_Lfun import TypeCheckLfun
from interpreter import interp_Lfun, interp_Cfun
import argparse
from utils import *
from compiler import Fun_Compiler

class Shrinked:

    def is_shrinked_exp(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case Call(f, args): 
                self.is_shrinked_exp(f)
                for a in args:
                    self.is_shrinked_exp(a)
                return True
            case BoolOp(op, [lhs, rhs]): # No need to recurse because it will always be false nonetheless
                raise Exception("error: shrinked program should not contain a BoolOp such as '" + str(op) + "':")
           
            case UnaryOp(op, e): return self.is_shrinked_exp(e)
            case BinOp(lhs, op, rhs):
                return self.is_shrinked_exp(lhs) and self.is_shrinked_exp(rhs)
            case Compare(lhs, [cmp], [rhs]):
                return self.is_shrinked_exp(lhs) and self.is_shrinked_exp(rhs)
            case IfExp(cond, thn, els):
                return self.is_shrinked_exp(cond) and \
                       self.is_shrinked_exp(thn) and \
                       self.is_shrinked_exp(els)
            case _:
                raise Exception("Shrinked: Unsupported expression " + repr(e))

    def is_shrinked_stmt(self, s):
        match s:
            case Return(e):
                return self.is_shrinked_exp(e)
            case FunctionDef(f, params, bod, dl, returns, comment):
                return self.are_shrinked_stmts(bod)
            case Expr(e):
                return self.is_shrinked_exp(e)
            case Assign([Name(var)], e):
                return self.is_shrinked_exp(e)
            case If(e, thn, els):
                return self.is_shrinked_exp(e) and \
                       self.are_shrinked_stmts(thn) and \
                       self.are_shrinked_stmts(els)
            case While(test, body, []):
                return self.is_shrinked_exp(test) and self.are_shrinked_stmts(body)
            case _:
                raise Exception("Shrinked: Unsupported statement " + repr(s))

    def are_shrinked_stmts(self, ss):
        for s in ss:
            if not self.is_shrinked_stmt(s):
                return False

        return True

    def is_shrinked(self, p):
        match p:
            case Module(ss):
                for s in ss[:-1]:
                    match s:
                        case FunctionDef(name, params, bod, dl, returns, comment):
                            self.are_shrinked_stmts(ss)
                        case _:
                            raise Exception("Shrinked: Top-level statements should only be function definitions")
                match ss[-1]:
                    case FunctionDef('main', _, bod, None, IntType(), None):
                        self.are_shrinked_stmts(ss)
                    case _:
                        raise Exception("Shrinked: Invalid last top-level statement, should be a 'main' function")
            case _:
                raise Exception('error in is_shrinked, invalid program ' + repr(p))

def is_shrinked(p):
    return Shrinked().is_shrinked(p)

class Revealed:

    def is_revealed_exp(self, e, funcs):
        match e:
            case Constant(c): return True
            case Name(v): 
                if v in funcs:
                    raise Exception("Revealed: Invalid Name(" + v + "), should be a FuncRef")
                return True
            case FunRef(label, i): 
                if label not in funcs:
                    raise Exception("Revealed: Invalid FuncRef(" + label + ", " + str(i) + "), should be a Name(" + label + ")")
                return True
            case Call(Name('input_int'), []):
                return True
            case Call(Name('print'), [e]):
                return self.is_revealed_exp(e, funcs)
            case Call(f, args):
                for e in args:
                    self.is_revealed_exp(e, funcs)
                return self.is_revealed_exp(f, funcs) 
            case UnaryOp(op, e): return self.is_revealed_exp(e, funcs)
            case BinOp(lhs, op, rhs):
                return self.is_revealed_exp(lhs, funcs) and self.is_revealed_exp(rhs, funcs)
            case Compare(lhs, [cmp], [rhs]):
                return self.is_revealed_exp(lhs, funcs) and self.is_revealed_exp(rhs, funcs)
            case IfExp(cond, thn, els):
                return self.is_revealed_exp(cond, funcs) and \
                       self.is_revealed_exp(thn, funcs) and \
                       self.is_revealed_exp(els, funcs)
            case _:
                raise Exception("Revealed: Unsupported expression " + repr(e))

    def is_revealed_stmt(self, s, funcs):
        match s:
            case FunctionDef(name, params, bod, dl, returns, comment):
                return self.are_revealed_stmts(bod, funcs)
            case Return(e):
                return self.is_revealed_exp(e, funcs)
            case While(e, ss, []):
                return self.is_revealed_exp(e, funcs) and self.are_revealed_stmts(ss, funcs)
            case Expr(e):
                return self.is_revealed_exp(e, funcs)
            case Assign([Name(var)], e):
                return self.is_revealed_exp(e, funcs)
            case If(e, thn, els):
                return self.is_revealed_exp(e, funcs) and \
                       self.are_revealed_stmts(thn, funcs) and \
                       self.are_revealed_stmts(els, funcs)
            case _:
                raise Exception("Revealed: Unsupported statement" + repr(s))

    def are_revealed_stmts(self, ss, funcs):
        for s in ss:
            if not self.is_revealed_stmt(s, funcs):
                return False

        return True

    def is_revealed(self, p):
        match p:
          case Module(body):
            funcs = set()
            for s in body:
                match s:
                    case FunctionDef(name, params, bod, dl, returns, comment):
                        funcs.add(name)
            self.are_revealed_stmts(body, funcs)
          case _:
            raise Exception('Revealed error, invalid program ' + repr(p))


def is_revealed(p):
    return Revealed().is_revealed(p)

class Monadic:

    def is_atom(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case _: return False

    def is_monadic_exp(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case FunRef(l,i): return True
            case Call(Name('input_int'), []): return True
            case BoolOp(op, [lhs, rhs]): # No need to recurse because it will always be false nonetheless
                raise Exception("Monadic: program should not contain a BoolOp such as '" + str(op) + "':")
           
            case Call(Name('print'), [e]):
                if self.is_atom(e): 
                    return True
                else:
                    raise Exception("Monadic: program should not contain a complex expression as argument of a print:")
            case Call(f, args):
                if not self.is_atom(f):
                    raise Exception("Monadic: program should not contain a complex expression as the target of a call:")
                if not all(map(lambda a: self.is_atom(a), args)): 
                    raise Exception("Monadic: program should not contain a complex expression as an argument of a call:")
                return True
            case UnaryOp(op, e):
                if self.is_atom(e): 
                    return True
                else:
                    raise Exception("Monadic: program should not contain a complex expression as argument of '" + op + "' :")
            case BinOp(lhs, op, rhs):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("Monadic: program should not contain complex expression(s) as argument of '" + op + "' :")
            case Compare(lhs, [cmp], [rhs]):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("Monadic: program should not contain complex expression(s) as argument of '" + cmp + "' :")
            case IfExp(cond, thn, els):
                return self.is_monadic_exp(cond) and \
                       self.is_monadic_exp(thn) and \
                       self.is_monadic_exp(els)
            case Begin(ss, e):
                return self.are_monadic_stmts(ss) and self.is_monadic_exp(e)
            case _:
                raise Exception("Monadic: Unsupported expression " + repr(e))

    def is_monadic_stmt(self, s):
        match s:
            case Expr(e):
                return self.is_monadic_exp(e)
            case Assign([Name(var)], e):
                return self.is_monadic_exp(e)
            case If(e, thn, els):
                return self.is_monadic_exp(e) and \
                       self.are_monadic_stmts(thn) and \
                       self.are_monadic_stmts(els)
            case While(e, ss, []):
                return self.is_monadic_exp(e) and \
                       self.are_monadic_stmts(ss) 
            case Return(a):
                if not self.is_atom(a):
                    raise Exception("Monadic: program should not contain a complex expression as parameter of a return stmt:")
                return True
            case FunctionDef(name, params, bod, dl, returns, comment):
                return self.are_monadic_stmts(bod)
            case _:
                raise Exception("Monadic: Unsupported statement " + repr(s))

    def are_monadic_stmts(self, ss):
        for s in ss:
            if not self.is_monadic_stmt(s):
                return False

        return True

    def is_monadic(self, p):
        match p:
            case Module(ss):
                return self.are_monadic_stmts(ss)
            case _:
                raise Exception('error in is_monadic, invalid program ' + repr(p))

def is_monadic(p):
    return Monadic().is_monadic(p)


class Cfun:
    def is_atom(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case _: return False

    def is_cfun_exp(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case FunRef(name,arity): return True
    
            case BoolOp(op, [lhs, rhs]): # No need to recurse because it will always be false nonetheless
                raise Exception("error: Cfun program should not contain a BoolOp such as '" + str(op) + "':")
           
            case UnaryOp(op, e):
                if self.is_atom(e): 
                    return True
                else:
                    raise Exception("error: Cfun program should not contain a complex expression as argument of '" + op + "' :")
            case BinOp(lhs, op, rhs):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("error: Cufn program should not contain complex expression(s) as argument of '" + op + "' :")
            case Compare(lhs, [cmp], [rhs]):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("error: Cfun program should not contain complex expression(s) as argument of '" + cmp + "' :")
            
            case Call(name,args): 
                if not self.is_cfun_exp(name): return False
                for arg in args:
                    if not self.is_cfun_exp(arg):return False
                return True
            
            case _:
                raise Exception("Cfun: Invalid Cif expression " + repr(e))

    def is_cfun_stmt(self, s):
        match s:
            case Expr(Call(Name('print'), [e])):
                if self.is_atom(e):
                    return True
                else:
                    raise Exception("error: Cfun program should not contain a complex expression as argument of 'print' :")
            case Expr(e):
                return self.is_cfun_exp(e)
            case Assign([Name(var)], e):
                return self.is_cfun_exp(e)
            case _:
                raise Exception("Cfun: Invalid Cfun statement" + repr(s))

    def is_tail(self, s):
        match s:
            case Return(e):
                return self.is_cfun_exp(e)
            case Goto(label):
                return True
            case If(Compare(a1, [cmp], [a2]), thn, els):
                return self.is_atom(a1) and self.is_atom(a2) and \
                       self.are_cfun_stmts(thn) and \
                       self.are_cfun_stmts(els)
            case _:
                raise Exception("Cfun: Invalid Cfun tail statement" + repr(s))

    def are_cfun_stmts(self, ss):
        match ss:
            case [*ss1, tail]: 
                for s in ss1:
                    if not self.is_cfun_stmt(s):
                        raise Exception("Cfun: invalid statement: " + repr(s))

                if not self.is_tail(tail):
                    raise Exception("Cfun: invalid tail statement: " + repr(tail))

                return True
            case _:
                raise Exception("Cfun: not a list of statements: " + repr(ss))
    def is_cfun_def(self,d):
        for (lbl,stms) in d.body.items():
            if not self.are_cfun_stmts(stms):return False
        return True

    def is_cfun(self, p):
        match p:
            case CProgramDefs(defs):
                for d in defs:
                    if not self.is_cfun_def(d):
                        raise Exception("Cfun: definition for " + repr(d))

                return True 
            case _:
                raise Exception('error in is_cfun, invalid program ' + repr(p))

def is_cfun(p):
    return Cfun().is_cfun(p)

def is_python_extension(filename):
    s = os.path.splitext(filename)
    if len(s) > 1:
        return s[1] == ".py"
    else:
        return False

def type_check_Lfun(p):
    TypeCheckLfun().type_check(p)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Command-line tester for compiler')
    parser.add_argument('path', type=str, help='Python program for a single test or directory of tests')
    parser.add_argument('--trace', action='store_true', help='Show intermediary representations and test results')
    parser.add_argument('--strict', action='store_true', help='Stop on first pass test failure', default=False)

    args = parser.parse_args()

    if args.trace:
        enable_tracing()

    interp_dict = {
        'shrink': interp_Lfun,
        'reveal_functions': interp_Lfun,
        'remove_complex_operands': interp_Lfun,
        'explicate_control' : interp_Cfun
    }

    type_checker_dict = {
        'source': type_check_Lfun,
        'shrink': is_shrinked,
        'reveal_functions': is_revealed,
        'remove_complex_operands': is_monadic,
        'explicate_control' : is_cfun
    }
    
    if os.path.isfile(args.path):
        (succ_passes, nb_passes, succ_test) = run_one_test(args.path, Fun_Compiler(), 'Fun_Compiler', type_checker_dict, interp_dict,stop_on_failed=args.strict)
        print("{}/{} passes successful".format(
            succ_passes, 
            nb_passes))
    elif os.path.isdir(args.path):
        run_tests(args.path, Fun_Compiler(), 'Fun_Compiler', type_checker_dict, interp_dict, stop_on_failed=args.strict)
    else:
        sys.stderr.write("Invalid path '" + args.path + "', neither a file or directory")
        sys.exit(1)

