# Exercise 8.4: LFun to x86 Compiler

Expand your compiler from Exercise 8.3 by implementing the `select_instructions`, `assign_homes`, `patch_instructions` and `prelude_and_conclusion` passes within `compiler.py`. Additionally, copy the tests you created for Exercise 8.3 into the tests folder.

Use the following script to run the tests:
```
    python3 run_tests.py <file_or_directory>
```

The expected output of the command `python3 run_tests.py tests/t0_mixture.py --trace` for the default compiler is:

```
test: tests/t0_mixture.py

# source program: t0_mixture

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))


Module([FunctionDef(f,<ast.arguments object at 0x7b8ed2701870>,[  Return(BinOp(Name('x'), Add(), Call(Name('step'), [])))]), FunctionDef(step,<ast.arguments object at 0x7b8ed2701630>,[  Return(Constant(1))]),   Assign([Name('i')], Call(Name('input_int'), [])),   Assign([Name('a')], Name('f')),   Expr(Call(Name('print'), [Call(Name('a'), [Call(Name('f'), [Name('i'), Constant(0)]), Constant(1)])]))])


# type checking source program


# shrink

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))



# reveal_functions



# remove_complex_operands

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))

Module([FunctionDef(f,[('x', IntType()), ('y', IntType())],[  Return(BinOp(Name('x'), Add(), Call(Name('step'), [])))]), FunctionDef(step,[],[  Return(Constant(1))]),   Assign([Name('i')], Call(Name('input_int'), [])),   Assign([Name('a')], Name('f')),   Expr(Call(Name('print'), [Call(Name('a'), [Call(Name('f'), [Name('i'), Constant(0)]), Constant(1)])]))])

# explicate_control

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))

Module([FunctionDef(f,[('x', IntType()), ('y', IntType())],[  Return(BinOp(Name('x'), Add(), Call(Name('step'), [])))]), FunctionDef(step,[],[  Return(Constant(1))]),   Assign([Name('i')], Call(Name('input_int'), [])),   Assign([Name('a')], Name('f')),   Expr(Call(Name('print'), [Call(Name('a'), [Call(Name('f'), [Name('i'), Constant(0)]), Constant(1)])]))])

# select_instructions

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))


# assign_homes

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))


# patch_instructions

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))


# prelude_and_conclusion

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))


tests/t0_mixture.s: Assembler messages:
tests/t0_mixture.s:1: Error: no such instruction: `def f(x:int,y:int)->int:'
tests/t0_mixture.s:2: Error: no such instruction: `return (x+step())'
tests/t0_mixture.s:4: Error: no such instruction: `def step()->int:'
tests/t0_mixture.s:5: Error: no such instruction: `return 1'
tests/t0_mixture.s:7: Error: junk at end of line, first unrecognized character is `('
tests/t0_mixture.s:9: Error: invalid character '(' in mnemonic
sh: 1: ./a.out: not found
0a1
> 8
\ No newline at end of file
Shrinked: Top-level statements should only be function definitions
    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))

Revealed: Invalid Name(step), should be a FuncRef
    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))

Monadic: program should not contain a complex expression as parameter of a return stmt:
    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))

error in is_cfun, invalid program Module([FunctionDef(f,[('x', IntType()), ('y', IntType())],[  Return(BinOp(Name('x'), Add(), Call(Name('step'), [])))]), FunctionDef(step,[],[  Return(Constant(1))]),   Assign([Name('i')], Call(Name('input_int'), [])),   Assign([Name('a')], Name('f')),   Expr(Call(Name('print'), [Call(Name('a'), [Call(Name('f'), [Name('i'), Constant(0)]), Constant(1)])]))])
    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))

compiler Fun_Compiler, executable failed on test tests/t0_mixture
0/5 passes successful, test failed

```

The expected output for a single file `tests/tests/t0_mixture.py` with a correct compiler is:
```
5/5 passes successful, test success
```

One valid output for a directory `tests` containing one test with a correct compiler is:

```
test: tests/t0_mixture.py

# source program: t0_mixture

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    i = input_int()
    a = f
    print(a(f(i, 0), 1))


Module([FunctionDef(f,<ast.arguments object at 0x75512903ed10>,[  Return(BinOp(Name('x'), Add(), Call(Name('step'), [])))]), FunctionDef(step,<ast.arguments object at 0x75512903ead0>,[  Return(Constant(1))]),   Assign([Name('i')], Call(Name('input_int'), [])),   Assign([Name('a')], Name('f')),   Expr(Call(Name('print'), [Call(Name('a'), [Call(Name('f'), [Name('i'), Constant(0)]), Constant(1)])]))])


# type checking source program


# shrink

    def f(x : int, y : int) -> int:
      return (x + step())

    def step() -> int:
      return 1

    def main() -> int:
      i = input_int()
      a = f
      print(a(f(i, 0), 1))
      return 0



shrinked program does not contain 'and' and 'or' BoolOps
compiler Fun_Compiler success on pass shrink on test
tests/t0_mixture

shrinked program maintains the expected input-output behaviour


# reveal_functions


functions were correctly revealed
compiler Fun_Compiler success on pass reveal_functions on test
tests/t0_mixture

function-revealed program maintains the expected input-output behaviour


# remove_complex_operands

    def f(x : int, y : int) -> int:
      fun.0 = {step}
      tmp.1 = fun.0()
      tmp.2 = (x + tmp.1)
      return tmp.2

    def step() -> int:
      return 1

    def main() -> int:
      i = input_int()
      a = {f}
      fun.3 = {f}
      tmp.4 = fun.3(i, 0)
      tmp.5 = a(tmp.4, 1)
      print(tmp.5)
      return 0


Module([FunctionDef(f,[('x', IntType()), ('y', IntType())],[  Assign([Name('fun.0')], FunRef(name='step', arity=0)),   Assign([Name('tmp.1')], Call(Name('fun.0'), [])),   Assign([Name('tmp.2')], BinOp(Name('x'), Add(), Name('tmp.1'))),   Return(Name('tmp.2'))]), FunctionDef(step,[],[  Return(Constant(1))]), FunctionDef(main,[],[  Assign([Name('i')], Call(Name('input_int'), [])),   Assign([Name('a')], FunRef(name='f', arity=2)),   Assign([Name('fun.3')], FunRef(name='f', arity=2)),   Assign([Name('tmp.4')], Call(Name('fun.3'), [Name('i'), Constant(0)])),   Assign([Name('tmp.5')], Call(Name('a'), [Name('tmp.4'), Constant(1)])),   Expr(Call(Name('print'), [Name('tmp.5')])),   Return(Constant(0))])])
monadic program contains the expected atomic expressions
compiler Fun_Compiler success on pass remove_complex_operands on test
tests/t0_mixture

monadic program maintains the expected input-output behaviour


# explicate_control

  def f(x : int, y : int) -> int:
fstart:
      fun.0 = {step}
      tmp.1 = fun.0()
      tmp.2 = (x + tmp.1)
      return tmp.2


  def step() -> int:
stepstart:
      return 1


  def main() -> int:
mainstart:
      i = input_int()
      a = {f}
      fun.3 = {f}
      tmp.4 = fun.3(i, 0)
      tmp.5 = a(tmp.4, 1)
      print(tmp.5)
      return 0



CProgramDefs(defs=[FunctionDef(f,[('x', IntType()), ('y', IntType())],{'fstart': [  Assign([Name('fun.0')], FunRef(name='step', arity=0)),   Assign([Name('tmp.1')], Call(Name('fun.0'), [])),   Assign([Name('tmp.2')], BinOp(Name('x'), Add(), Name('tmp.1'))),   Return(Name('tmp.2'))]}), FunctionDef(step,[],{'stepstart': [  Return(Constant(1))]}), FunctionDef(main,[],{'mainstart': [  Assign([Name('i')], Call(Name('input_int'), [])),   Assign([Name('a')], FunRef(name='f', arity=2)),   Assign([Name('fun.3')], FunRef(name='f', arity=2)),   Assign([Name('tmp.4')], Call(Name('fun.3'), [Name('i'), Constant(0)])),   Assign([Name('tmp.5')], Call(Name('a'), [Name('tmp.4'), Constant(1)])),   Expr(Call(Name('print'), [Name('tmp.5')])),   Return(Constant(0))]})])
cif program contains the expected statements and expressions
compiler Fun_Compiler success on pass explicate_control on test
tests/t0_mixture

cif program maintains the expected input-output behaviour


# select_instructions

  def f() -> int:
fstart:
      movq %rdi, x
      movq %rsi, y
      leaq step(%rip), fun.0
      callq *fun.0
      movq %rax, tmp.1
      movq x, tmp.2
      addq tmp.1, tmp.2
      movq tmp.2, %rax
      jmp fconclusion


  def step() -> int:
stepstart:
      movq $1, %rax
      jmp stepconclusion


  def main() -> int:
mainstart:
      callq read_int
      movq %rax, i
      leaq f(%rip), a
      leaq f(%rip), fun.3
      movq i, %rdi
      movq $0, %rsi
      callq *fun.3
      movq %rax, tmp.4
      movq tmp.4, %rdi
      movq $1, %rsi
      callq *a
      movq %rax, tmp.5
      movq tmp.5, %rdi
      callq print_int
      movq $0, %rax
      jmp mainconclusion



# assign_homes

  def f() -> int:
fstart:
      movq %rdi, -8(%rbp)
      movq %rsi, -16(%rbp)
      leaq step(%rip), -24(%rbp)
      callq *-24(%rbp)
      movq %rax, -32(%rbp)
      movq -8(%rbp), -40(%rbp)
      addq -32(%rbp), -40(%rbp)
      movq -40(%rbp), %rax
      jmp fconclusion


  def step() -> int:
stepstart:
      movq $1, %rax
      jmp stepconclusion


  def main() -> int:
mainstart:
      callq read_int
      movq %rax, -8(%rbp)
      leaq f(%rip), -16(%rbp)
      leaq f(%rip), -24(%rbp)
      movq -8(%rbp), %rdi
      movq $0, %rsi
      callq *-24(%rbp)
      movq %rax, -32(%rbp)
      movq -32(%rbp), %rdi
      movq $1, %rsi
      callq *-16(%rbp)
      movq %rax, -40(%rbp)
      movq -40(%rbp), %rdi
      callq print_int
      movq $0, %rax
      jmp mainconclusion



# patch_instructions

  def f() -> int:
fstart:
      movq %rdi, -8(%rbp)
      movq %rsi, -16(%rbp)
      leaq step(%rip), %rax
      movq %rax, -24(%rbp)
      callq *-24(%rbp)
      movq %rax, -32(%rbp)
      movq -8(%rbp), %rax
      movq %rax, -40(%rbp)
      movq -32(%rbp), %rax
      addq %rax, -40(%rbp)
      movq -40(%rbp), %rax
      jmp fconclusion


  def step() -> int:
stepstart:
      movq $1, %rax
      jmp stepconclusion


  def main() -> int:
mainstart:
      callq read_int
      movq %rax, -8(%rbp)
      leaq f(%rip), %rax
      movq %rax, -16(%rbp)
      leaq f(%rip), %rax
      movq %rax, -24(%rbp)
      movq -8(%rbp), %rdi
      movq $0, %rsi
      callq *-24(%rbp)
      movq %rax, -32(%rbp)
      movq -32(%rbp), %rdi
      movq $1, %rsi
      callq *-16(%rbp)
      movq %rax, -40(%rbp)
      movq -40(%rbp), %rdi
      callq print_int
      movq $0, %rax
      jmp mainconclusion



# prelude_and_conclusion

	.align 16
fstart:
    movq %rdi, -8(%rbp)
    movq %rsi, -16(%rbp)
    leaq step(%rip), %rax
    movq %rax, -24(%rbp)
    callq *-24(%rbp)
    movq %rax, -32(%rbp)
    movq -8(%rbp), %rax
    movq %rax, -40(%rbp)
    movq -32(%rbp), %rax
    addq %rax, -40(%rbp)
    movq -40(%rbp), %rax
    jmp fconclusion

	.align 16
f:
    pushq %rbp
    movq %rsp, %rbp
    subq $48, %rsp
    jmp fstart

	.align 16
fconclusion:
    addq $48, %rsp
    popq %rbp
    retq 

	.align 16
stepstart:
    movq $1, %rax
    jmp stepconclusion

	.align 16
step:
    pushq %rbp
    movq %rsp, %rbp
    subq $0, %rsp
    jmp stepstart

	.align 16
stepconclusion:
    addq $0, %rsp
    popq %rbp
    retq 

	.align 16
mainstart:
    callq read_int
    movq %rax, -8(%rbp)
    leaq f(%rip), %rax
    movq %rax, -16(%rbp)
    leaq f(%rip), %rax
    movq %rax, -24(%rbp)
    movq -8(%rbp), %rdi
    movq $0, %rsi
    callq *-24(%rbp)
    movq %rax, -32(%rbp)
    movq -32(%rbp), %rdi
    movq $1, %rsi
    callq *-16(%rbp)
    movq %rax, -40(%rbp)
    movq -40(%rbp), %rdi
    callq print_int
    movq $0, %rax
    jmp mainconclusion

	.globl main
	.align 16
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $48, %rsp
    jmp mainstart

	.align 16
mainconclusion:
    addq $48, %rsp
    popq %rbp
    retq 




tests: 1/1 for compiler Fun_Compiler
passes: 5/5 for compiler Fun_Compiler

```

As for previous weeks, you may show intermediary outputs with `--trace` and stop on the first error with `--strict`.
