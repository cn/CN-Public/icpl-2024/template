# Exercises E6.2

Complete the `While_Compiler` within `compiler.py`, which compiles L<sub>while</sub> programs into
x86<sub>If</sub>. To achieve this, first copy your implemntation of `If_Compiler` from Week six, and then implement the three incomplete functions in the  `While_Compiler` class: `shrink_stmt`, `rco_stmt` and `explicate_stmt`.  


Similar to previous weeks, provide five **valid** L<sub>while</sub> test programs for different scenarios, including nested loops. Then test your compiler on all of these programs and ensure that the input-output behaviour of the
program is consistent whether it runs on the L<sub>while</sub> interpreter or natively on `x86`. To acomplish this, use the provided script `run_tests.py`. We will use a copy of the same script to grade your tests and compiler. If you find a bug or
encounter issues in `run_tests.py`, please share them (early) on the Discord forum and we will update the template as necessary. See usage example below:

The expected output for a single file `tests/t0_and.py` with the default template compiler is using `python3 run_tests.py tests/t0_sum_of_first_pos_ints.py` command is:

```
error: shrinked program should not contain a BoolOp such as 'and':
    sum = ((0 + 1) - 1)
    i = input_int()
    while (i > 0 and True):
      sum = (sum + i)
      i = (i - 1)
    print(sum)

can only concatenate str (not "Sub") to str
    sum = ((0 + 1) - 1)
    i = input_int()
    while (i > 0 and True):
      sum = (sum + i)
      i = (i - 1)
    print(sum)

can only concatenate str (not "Sub") to str
start:
    sum = ((0 + 1) - 1)
    i = input_int()
    while (i > 0 and True):
      sum = (sum + i)
      i = (i - 1)
    print(sum)
    return 0


tests/t0_sum_of_first_pos_ints.s: Assembler messages:
tests/t0_sum_of_first_pos_ints.s: Warning: end of file not at end of a line; newline inserted
tests/t0_sum_of_first_pos_ints.s:1: Error: no such instruction: `none'
sh: 1: ./a.out: not found
0a1
> 15
compiler While_Compiler, executable failed on test tests/t0_sum_of_first_pos_ints
0/4 passes successful, test failed

```

One possible valid output for a single file `tests/t0_sum_of_first_pos_ints.py` with a correct compiler and the `--trace` option is:
```
test: tests/t0_sum_of_first_pos_ints.py

# source program: t0_sum_of_first_pos_ints

    sum = ((0 + 1) - 1)
    i = input_int()
    while (i > 0 and True):
      sum = (sum + i)
      i = (i - 1)
    print(sum)


Module([  Assign([Name('sum')], BinOp(BinOp(Constant(0), Add(), Constant(1)), Sub(), Constant(1))),   Assign([Name('i')], Call(Name('input_int'), [])), While(BoolOp(Compare(Name('i'), [Gt()], [Constant(0)]), And(), Constant(True)), [  Assign([Name('sum')], BinOp(Name('sum'), Add(), Name('i'))),   Assign([Name('i')], BinOp(Name('i'), Sub(), Constant(1)))], []),   Expr(Call(Name('print'), [Name('sum')]))])


# type checking source program


# shrink

    sum = ((0 + 1) - 1)
    i = input_int()
    while (True if i > 0 else False):
      sum = (sum + i)
      i = (i - 1)
    print(sum)


shrinked program does not contain 'and' and 'or' BoolOps
compiler While_Compiler success on pass shrink on test
tests/t0_sum_of_first_pos_ints

shrinked program maintains the expected input-output behaviour


# remove_complex_operands

    tmp.0 = (0 + 1)
    sum = (tmp.0 - 1)
    i = input_int()
    while (True if i > 0 else False):
      sum = (sum + i)
      i = (i - 1)
    print(sum)

Module([  Assign([Name('tmp.0')], BinOp(Constant(0), Add(), Constant(1))),   Assign([Name('sum')], BinOp(Name('tmp.0'), Sub(), Constant(1))),   Assign([Name('i')], Call(Name('input_int'), [])), While(IfExp(Compare(Name('i'), [Gt()], [Constant(0)]), Constant(True), Constant(False)), [  Assign([Name('sum')], BinOp(Name('sum'), Add(), Name('i'))),   Assign([Name('i')], BinOp(Name('i'), Sub(), Constant(1)))], []),   Expr(Call(Name('print'), [Name('sum')]))])
monadic program contains the expected atomic expressions
compiler While_Compiler success on pass remove_complex_operands on test
tests/t0_sum_of_first_pos_ints

monadic program maintains the expected input-output behaviour


# explicate_control

block.2:
    sum = (sum + i)
    i = (i - 1)
    goto loop.1

block.3:
    print(sum)
    return 0

loop.1:
    if i > 0:
      goto block.2
    else:
      goto block.3

start:
    tmp.0 = (0 + 1)
    sum = (tmp.0 - 1)
    i = input_int()
    goto loop.1


CProgram(body={'block.2': [  Assign([Name('sum')], BinOp(Name('sum'), Add(), Name('i'))),   Assign([Name('i')], BinOp(Name('i'), Sub(), Constant(1))), Goto(label='loop.1')], 'block.3': [  Expr(Call(Name('print'), [Name('sum')])),   Return(Constant(0))], 'loop.1': [If(Compare(Name('i'), [Gt()], [Constant(0)]), [Goto(label='block.2')], [Goto(label='block.3')])], 'start': [  Assign([Name('tmp.0')], BinOp(Constant(0), Add(), Constant(1))),   Assign([Name('sum')], BinOp(Name('tmp.0'), Sub(), Constant(1))),   Assign([Name('i')], Call(Name('input_int'), [])), Goto(label='loop.1')]})
cif program contains the expected statements and expressions
compiler While_Compiler success on pass explicate_control on test
tests/t0_sum_of_first_pos_ints

cif program maintains the expected input-output behaviour


# select_instructions

	.align 16
block.2:
    addq i, sum
    subq $1, i
    jmp loop.1

	.align 16
block.3:
    movq sum, %rdi
    callq print_int
    movq $0, %rax
    jmp conclusion

	.align 16
loop.1:
    cmpq $0, i
    jg block.2
    jmp block.3

	.align 16
start:
    movq $0, tmp.0
    addq $1, tmp.0
    movq tmp.0, sum
    subq $1, sum
    callq read_int
    movq %rax, i
    jmp loop.1




# assign_homes

	.align 16
block.2:
    addq -8(%rbp), -16(%rbp)
    subq $1, -8(%rbp)
    jmp loop.1

	.align 16
block.3:
    movq -16(%rbp), %rdi
    callq print_int
    movq $0, %rax
    jmp conclusion

	.align 16
loop.1:
    cmpq $0, -8(%rbp)
    jg block.2
    jmp block.3

	.align 16
start:
    movq $0, -24(%rbp)
    addq $1, -24(%rbp)
    movq -24(%rbp), -16(%rbp)
    subq $1, -16(%rbp)
    callq read_int
    movq %rax, -8(%rbp)
    jmp loop.1




# patch_instructions

	.align 16
block.2:
    movq -8(%rbp), %rax
    addq %rax, -16(%rbp)
    subq $1, -8(%rbp)
    jmp loop.1

	.align 16
block.3:
    movq -16(%rbp), %rdi
    callq print_int
    movq $0, %rax
    jmp conclusion

	.align 16
loop.1:
    cmpq $0, -8(%rbp)
    jg block.2
    jmp block.3

	.align 16
start:
    movq $0, -24(%rbp)
    addq $1, -24(%rbp)
    movq -24(%rbp), %rax
    movq %rax, -16(%rbp)
    subq $1, -16(%rbp)
    callq read_int
    movq %rax, -8(%rbp)
    jmp loop.1




# prelude_and_conclusion

	.align 16
block.2:
    movq -8(%rbp), %rax
    addq %rax, -16(%rbp)
    subq $1, -8(%rbp)
    jmp loop.1

	.align 16
block.3:
    movq -16(%rbp), %rdi
    callq print_int
    movq $0, %rax
    jmp conclusion

	.align 16
loop.1:
    cmpq $0, -8(%rbp)
    jg block.2
    jmp block.3

	.align 16
start:
    movq $0, -24(%rbp)
    addq $1, -24(%rbp)
    movq -24(%rbp), %rax
    movq %rax, -16(%rbp)
    subq $1, -16(%rbp)
    callq read_int
    movq %rax, -8(%rbp)
    jmp loop.1

	.globl main
	.align 16
main:
    pushq %rbp
    movq %rsp, %rbp
    subq $32, %rsp
    jmp start

	.align 16
conclusion:
    addq $32, %rsp
    popq %rbp
    retq 




4/4 passes successful, test success

```

The expected output for a directory `tests` containing two tests with a correct compiler (without the `--trace` option) is:
```
tests: 2/2 for compiler While_Compiler
passes: 8/8 for compiler While_Compiler
```

As in previous weeks, you can display intermediate outputs using --trace and halt on the first error with --strict.

# Common Mistakes and Solutions

`
Traceback (most recent call last):
  File "/home/ali/Desktop/tmp/Week7/E6-2/run_tests.py", line 8, in <module>
    from compiler import While_Compiler
ImportError: cannot import name 'While_Compiler' from 'compiler' (/home/template/Week7/E6-2/compiler.py)
`

The name of your compiler is not 'While\_Compiler' as expected. Modify it.
