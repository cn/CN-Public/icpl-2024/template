import sys
import os
import traceback
from interpreter import interp_Lwhile, interp_Cif
from type_check_Lwhile import TypeCheckLwhile 
import argparse
from utils import *
from compiler import While_Compiler

class Shrinked:
    def is_shrinked_exp(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case Call(Name('input_int'), []): return True
            case BoolOp(op, [lhs, rhs]): # No need to recurse because it will always be false nonetheless
                raise Exception("error: shrinked program should not contain a BoolOp such as '" + str(op) + "':")
           
            case Call(Name('print'), [e]): return self.is_shrinked_exp(e)
            case UnaryOp(op, e): return self.is_shrinked_exp(e)
            case BinOp(lhs, op, rhs):
                return self.is_shrinked_exp(lhs) and self.is_shrinked_exp(rhs)
            case Compare(lhs, [cmp], [rhs]):
                return self.is_shrinked_exp(lhs) and self.is_shrinked_exp(rhs)
            case IfExp(cond, thn, els):
                return self.is_shrinked_exp(cond) and \
                       self.is_shrinked_exp(thn) and \
                       self.is_shrinked_exp(els)
            case _:
                raise Exception("Shrinked: Unsupported expression " + repr(e))

    def is_shrinked_stmt(self, s):
        match s:
            case Expr(e):
                return self.is_shrinked_exp(e)
            case Assign([Name(var)], e):
                return self.is_shrinked_exp(e)
            case If(e, thn, els):
                return self.is_shrinked_exp(e) and \
                       self.are_shrinked_stmts(thn) and \
                       self.are_shrinked_stmts(els)
            case While(test, body, []):
                return self.is_shrinked_exp(test) and self.are_shrinked_stmts(body)
            case _:
                raise Exception("Shrinked: Unsupported statement" + repr(s))

    def are_shrinked_stmts(self, ss):
        for s in ss:
            if not self.is_shrinked_stmt(s):
                return False

        return True

    def is_shrinked(self, p):
        match p:
            case Module(ss):
                return self.are_shrinked_stmts(ss)
            case _:
                raise Exception('error in is_shrinked, invalid program ' + repr(p))

def is_shrinked(p):
    return Shrinked().is_shrinked(p)

class Monadic:
    def is_atom(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case _: return False

    def is_monadic_exp(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case Call(Name('input_int'), []): return True
            case BoolOp(op, [lhs, rhs]): # No need to recurse because it will always be false nonetheless
                raise Exception("error: monadic program should not contain a BoolOp such as '" + str(op) + "':")
           
            case Call(Name('print'), [e]):
                if self.is_atom(e): 
                    return True
                else:
                    raise Exception("error: monadic program should not contain a complex expression as argument of a print:")
            case UnaryOp(op, e):
                if self.is_atom(e): 
                    return True
                else:
                    raise Exception("error: monadic program should not contain a complex expression as argument of '" + op + "' :")
            case BinOp(lhs, op, rhs):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("error: monadic program should not contain complex expression(s) as argument of '" + op + "' :")
            case Compare(lhs, [cmp], [rhs]):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("error: monadic program should not contain complex expression(s) as argument of '" + cmp + "' :")
            case IfExp(cond, thn, els):
                return self.is_monadic_exp(cond) and \
                       self.is_monadic_exp(thn) and \
                       self.is_monadic_exp(els)
            case Begin(ss, e):
                return self.are_monadic_stmts(ss) and self.is_monadic_exp(e)
            case _:
                raise Exception("Monadic: Unsupported expression " + repr(e))

    def is_monadic_stmt(self, s):
        match s:
            case Expr(e):
                return self.is_monadic_exp(e)
            case Assign([Name(var)], e):
                return self.is_monadic_exp(e)
            case If(e, thn, els):
                return self.is_monadic_exp(e) and \
                       self.are_monadic_stmts(thn) and \
                       self.are_monadic_stmts(els)
            case While(test, body, []):
                return self.is_monadic_exp(test) and \
                self.are_monadic_stmts(body)
            case _:
                raise Exception("Monadic: Unsupported statement" + repr(s))

    def are_monadic_stmts(self, ss):
        for s in ss:
            if not self.is_monadic_stmt(s):
                return False

        return True

    def is_monadic(self, p):
        match p:
            case Module(ss):
                return self.are_monadic_stmts(ss)
            case _:
                raise Exception('error in is_monadic, invalid program ' + repr(p))

class Cif:
    def is_atom(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case _: return False

    def is_cif_exp(self, e):
        match e:
            case Constant(c): return True
            case Name(v): return True
            case Call(Name('input_int'), []): return True
            case BoolOp(op, [lhs, rhs]): # No need to recurse because it will always be false nonetheless
                raise Exception("error: Cif program should not contain a BoolOp such as '" + str(op) + "':")
           
            case UnaryOp(op, e):
                if self.is_atom(e): 
                    return True
                else:
                    raise Exception("error: Cif program should not contain a complex expression as argument of '" + op + "' :")
            case BinOp(lhs, op, rhs):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("error: Cif program should not contain complex expression(s) as argument of '" + op + "' :")
            case Compare(lhs, [cmp], [rhs]):
                if self.is_atom(lhs) and self.is_atom(rhs): 
                    return True
                else:
                    raise Exception("error: Cif program should not contain complex expression(s) as argument of '" + cmp + "' :")
            case _:
                raise Exception("Cif: Invalid Cif expression " + repr(e))

    def is_cif_stmt(self, s):
        match s:
            case Expr(Call(Name('print'), [e])):
                if self.is_atom(e):
                    return True
                else:
                    raise Exception("error: Cif program should not contain a complex expression as argument of 'print' :")
            case Expr(e):
                return self.is_cif_exp(e)
            case Assign([Name(var)], e):
                return self.is_cif_exp(e)
            case _:
                raise Exception("Cif: Invalid Cif statement" + repr(s))

    def is_tail(self, s):
        match s:
            case Return(e):
                return self.is_cif_exp(e)
            case Goto(label):
                return True
            case If(Compare(a1, [cmp], [a2]), thn, els):
                return self.is_atom(a1) and self.is_atom(a2) and \
                       self.are_cif_stmts(thn) and \
                       self.are_cif_stmts(els)
            case _:
                raise Exception("Cif: Invalid Cif tail statement" + repr(s))

    def are_cif_stmts(self, ss):
        match ss:
            case [*ss1, tail]: 
                for s in ss1:
                    if not self.is_cif_stmt(s):
                        raise Exception("Cif: invalid statement: " + repr(s))

                if not self.is_tail(tail):
                    raise Exception("Cif: invalid tail statement: " + repr(tail))

                return True
            case _:
                raise Exception("Cif: not a list of statements: " + repr(ss))

    def is_cif(self, p):
        match p:
            case CProgram(blocks):
                for label, ss in blocks.items():
                    if not self.are_cif_stmts(ss):
                        raise Exception("Cif: invalid statements for block '" + label + "': " + repr(ss))

                return True 
            case _:
                raise Exception('error in is_cif, invalid program ' + repr(p))


def is_cif(p):
    return Cif().is_cif(p)

def is_monadic(p):
    return Monadic().is_monadic(p)

def is_python_extension(filename):
    s = os.path.splitext(filename)
    if len(s) > 1:
        return s[1] == ".py"
    else:
        return False

def type_check(p):
    match p:
        case Module(body):
                TypeCheckLwhile().type_check_stmts(body, {})
        case _:
            raise Exception("Invalid program " + repr(p))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Command-line tester for compiler')
    parser.add_argument('path', type=str, help='Python program for a single test or directory of tests')
    parser.add_argument('--trace', action='store_true', help='Show intermediary representations and test results')
    parser.add_argument('--strict', action='store_true', help='Stop on first pass test failure', default=False)

    if not os.path.isfile('runtime.o'):
        sys.stderr.write("runtime.o: not found, please compile runtime.c first (ex: gcc -c runtime.c -o runtime.o )\n")
        sys.exit(1)

    if os.path.isfile('a.out'):
        sys.stderr.write("a.out left from previous test run, please remove first.\n")
        sys.exit(1)

    args = parser.parse_args()

    if args.trace:
        enable_tracing()

    interp_dict = {
        'shrink': interp_Lwhile,
        'remove_complex_operands': interp_Lwhile,
        'explicate_control': interp_Cif
    }

    type_checker_dict = {
        'source': type_check,
        'shrink': is_shrinked,
        'remove_complex_operands': is_monadic,
        'explicate_control': is_cif
    }

    if os.path.isfile(args.path):
        (succ_passes, nb_passes, succ_test) = run_one_test(args.path, While_Compiler(), 'While_Compiler', type_checker_dict, interp_dict, stop_on_failed=args.strict)
        print("{}/{} passes successful, test {}".format(
            succ_passes, 
            nb_passes, 
            "success" if succ_test == 1 else "failed"))
    elif os.path.isdir(args.path):
        run_tests(args.path, While_Compiler(), 'While_Compiler', type_checker_dict, interp_dict, stop_on_failed=args.strict)
    else:
        sys.stderr.write("Invalid path '" + args.path + "', neither a file or directory")
        sys.exit(1)

