	.align 16
_block.2:
    addq $1, -8(%rbp)
    movq -16(%rbp), %rax
    movq %rax, -24(%rbp)
    jmp _loop.1

	.align 16
_block.3:
    movq -24(%rbp), %rdi
    callq _print_int
    movq $0, %rax
    jmp _conclusion

	.align 16
_loop.1:
    movq -8(%rbp), %rax
    movq %rax, -32(%rbp)
    movq -40(%rbp), %rax
    movq %rax, -32(%rbp)
    cmpq $0, -32(%rbp)
    jl _block.2
    jmp _block.3

	.align 16
_start:
    movq $0, -8(%rbp)
    callq _read_int
    movq %rax, -16(%rbp)
    callq _read_int
    movq %rax, -40(%rbp)
    movq $0, -24(%rbp)
    jmp _loop.1

	.globl _main
	.align 16
_main:
    pushq %rbp
    movq %rsp, %rbp
    subq $48, %rsp
    jmp _start

	.align 16
_conclusion:
    addq $48, %rsp
    popq %rbp
    retq 


