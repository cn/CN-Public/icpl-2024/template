from type_check_Lwhile import TypeCheckLwhile
import sys
import os
from utils import *

def is_python_extension(filename):
    s = os.path.splitext(filename)
    if len(s) > 1:
        return s[1] == ".py"
    else:
        return False

def typetest(path, mustBeCorrect):
    with open(path, 'r') as f:
        p = parse(f.read())
        match p:
            case Module(body):
                try:
                    TypeCheckLwhile().type_check_stmts(body, {})
                    if mustBeCorrect:
                        print(path + ': valid')
                    else:
                        print(path + ': invalid')
                except Exception as e:
                    if not mustBeCorrect:
                        print(path + ': valid')
                    else:
                        print(path + ': invalid')
                        print(e)
            case _:
                raise Exception("Invalid program " + repr(p))

if __name__ == '__main__':
    path = sys.argv[1]

    if os.path.isfile(path): 
        typetest(path, False if path.find("incorrect") >= 0 else True)
    elif os.path.isdir(path):
        for (root, dirnames, filenames) in os.walk(path):
            if set(dirnames) != {"correct", "incorrect"}:
                print("run_type_checker: Invalid directory, expected a directory with two sub-directories: 'correct' and 'incorrect'")
                sys.exit(1)

            for (dirpath, dirnames, filenames) in os.walk(os.path.join(root,"correct")):
                tests = filter(is_python_extension, filenames)
                tests = [os.path.join(dirpath, t) for t in tests]
                for test in tests:
                    typetest(test, True)

            for (dirpath, dirnames, filenames) in os.walk(os.path.join(root,"incorrect")):
                tests = filter(is_python_extension, filenames)
                tests = [os.path.join(dirpath, t) for t in tests]
                for test in tests:
                    typetest(test, False)

            break 
