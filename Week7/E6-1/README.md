# Exercise 6.1: Type Checker for Lwhile

Read the type checker code, `type_check_Lwhile.py`, and provide original tests, i.e. not found in the textbook and devised independently of other teams, that exercise the only new language feature of `Lwhile`, i.e `while` statement:

1. 3 examples, under `tests/correct/t{team_number}_{short_description}.py`, that correctly type check.
2. 3 examples, under `tests/incorrect/t{team_number}_{short_description}.py`, that result in type errors.

Use the following script to run the type checker: 
```
    python3 run_type_checker.py <file_or_directory>
```

The expected output for a single file `tests/correct/t0_sum_of_first_pos_ints.py` is:
```
    tests/correct/t0_sum_of_first_pos_ints.py: valid

```

The expected output for a directory `tests` containing two sub-directories 'correct' and 'incorrect' is:
```
    tests/correct/t0_sum_of_first_pos_ints.py: valid
    tests/incorrect/t0_non_bool_condition.py: valid

```
